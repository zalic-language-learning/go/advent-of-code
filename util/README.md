# util

## install

```
go get gitlab.com/zalic-language-learning/go/advent-of-code/util
```

## utils

### ReadFileAsLines

```go
package main

import (
        "fmt"

        "gitlab.com/zalic-language-learning/go/advent-of-code/util"
)

func main() {
        filename := "input"
        lines := make(chan string)

        go util.ReadFileAsLines(filename, lines)

        for line := range lines {
                fmt.Println(line)
        }
}
```

### ReadFileAsRunes
```go
package main

import (
        "fmt"

        "gitlab.com/zalic-language-learning/go/advent-of-code/util"
)

func main() {
        filename := "input"
        runes := make(chan rune)

        go util.ReadFileAsRunes(filename, runes)

        for rune := range runes {
                fmt.Println(string(rune))
        }
}
```
