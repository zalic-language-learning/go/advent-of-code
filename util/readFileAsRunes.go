package util

import (
	"bufio"
	"io"
	"log"
	"os"
)

func ReadFileAsRunes(file string, runes chan<- rune) {
	dir, _ := os.Getwd()
	filename := dir + "/" + file

	f, err := os.Open(filename)

	if err != nil {
		panic(err)
	}

	defer f.Close()

	fileScanner := bufio.NewReader(f)

	for {
		if rune, _, err := fileScanner.ReadRune(); err != nil {
			if err == io.EOF {
				break
			} else {
				log.Fatal(err)
			}
		} else {
			runes <- rune
		}
	}

	close(runes)
}
