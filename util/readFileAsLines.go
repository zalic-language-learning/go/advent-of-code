package util

import (
	"bufio"
	"os"
)

func ReadFileAsLines(file string, lines chan<- string) {
	dir, _ := os.Getwd()
	filename := dir + "/" + file

	f, err := os.Open(filename)

	if err != nil {
		panic(err)
	}

	defer f.Close()

	fileScanner := bufio.NewScanner(f)
	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		line := fileScanner.Text()
		lines <- line
	}

	close(lines)
}
