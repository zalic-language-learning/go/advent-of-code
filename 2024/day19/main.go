package main

import (
	"fmt"
	"os"
	"strings"
)

type Puzzle struct {
	towels  []string
	designs []string
}

func parseInput(filename string) *Puzzle {
	input, err := os.ReadFile(filename)

	if err != nil {
		panic(err)
	}

	lines := strings.Split(string(input), "\n")

	puzzle := Puzzle{towels: strings.Split(lines[0], ", ")}

	for _, line := range lines[2:] {
		if line == "" {
			continue
		}
		puzzle.designs = append(puzzle.designs, line)
	}

	return &puzzle
}

// my recursive function magnitudes slower for obvious reasons
func (puzzle *Puzzle) countDesigns(design string) int {
	count := 0
	dl := len(design)
	for _, towel := range puzzle.towels {
		tl := len(towel)
		index := dl - tl
		if dl >= tl && design[index:] == towel {
			if dl == tl {
				count++
			} else {
				count += puzzle.countDesigns(design[:index])
			}
		}
	}
	return count
}

// https://github.com/Ahmedn1/AoC/blob/main/advent_of_code_2024/19/main.go
func countWaysToCreateDesign(patterns []string, design string) int {
	n := len(design)
	dp := make([]int, n+1) // dp[i] = number of ways to create design[0:i]
	dp[0] = 1              // Base case: 1 way to form an empty string

	for i := 1; i <= n; i++ {
		for _, pattern := range patterns {
			patternLength := len(pattern)
			if i >= patternLength && design[i-patternLength:i] == pattern {
				dp[i] += dp[i-patternLength]
			}
		}
	}

	return dp[n] // The number of ways to form the entire design
}

func (puzzle *Puzzle) run() (part1, part2 int) {
	for _, design := range puzzle.designs {
		// count := puzzle.countDesigns(design)
		count := countWaysToCreateDesign(puzzle.towels, design)
		if count > 0 {
			part1++
		}
		part2 += count
	}
	return
}

func main() {
	// puzzle := parseInput("input_demo")
	puzzle := parseInput("input")
	part1, part2 := puzzle.run()
	fmt.Println("part1", part1)
	fmt.Println("part2", part2)
}
