package main

import (
	"fmt"
)

type Direction int

const (
	UP = iota
	RIGHT
	DOWN
	LEFT
)

var DirectionValue = map[Direction][2]int{
	UP:    {-1, 0},
	DOWN:  {1, 0},
	LEFT:  {0, -1},
	RIGHT: {0, 1},
}

type Level struct {
	obstacles     []map[int]bool
	startPosition [2]int
	height        int
	width         int
}

type Game struct {
	level  *Level
	walked []map[int]bool
	// path      [][2]int
	hits      map[Direction][]map[int]bool
	obstacles []map[int]bool
	position  [2]int
	direction Direction
}

// alternatively could have kept only one struct and done something like
// func (game *Game) reset() {
// 	game.walked = nil
// 	for i := 0; i < game.level.height; i++ {
// 		game.walked = append(game.walked, make(map[int]bool))
// 	}
// 	game.position = game.level.startPosition
// 	game.direction = UP
// }

func (game *Game) init() {
	for i := 0; i < game.level.height; i++ {
		game.walked = append(game.walked, make(map[int]bool))
	}
	for i := 0; i < game.level.height; i++ {
		game.obstacles = append(game.obstacles, make(map[int]bool))
	}
	game.hits = make(map[Direction][]map[int]bool)
	for direction := range DirectionValue {
		var loop []map[int]bool
		for i := 0; i < game.level.height; i++ {
			loop = append(loop, make(map[int]bool))
		}
		game.hits[direction] = loop

	}
	game.position = game.level.startPosition
}

func (game *Game) nextPos(direction Direction) [2]int {
	modification := DirectionValue[direction]
	return [2]int{
		game.position[0] + modification[0],
		game.position[1] + modification[1],
	}
}

// brute force loop checking by checking if two same length
// last chunks of the slice have same values
// func (game *Game) isLoop() bool {
// 	l := len(game.path)
// 	if l < 4 {
// 		return false
// 	}
// 	for i := 3; i < l/2; i++ {
// 		x1 := l - i
// 		x2 := l - i*2
// 		s1 := game.path[x1 : x1+i]
// 		s2 := game.path[x2 : x2+i]
// 		match := true
// 		for ii := 0; ii < i; ii++ {
// 			if s1[ii] != s2[ii] {
// 				match = false
// 				break
// 			}
// 		}
// 		if match {
// 			return true
// 		}
// 	}
// 	return false
// }

// using named return values
// return values:
// ok
//   - if true, can take a next step after this one,
//   - if false, then either path is looping or the position has moved out of bounds
//
// is_loop - true when we know the path is looping
func (game *Game) step() (ok bool, is_loop bool) {
	game.walked[game.position[0]][game.position[1]] = true
	// game.path = append(game.path, [2]int{game.position[0], game.position[1]})

	n := game.nextPos(game.direction)

	if n[0] < 0 || n[0] >= game.level.height {
		// fmt.Println("out of bounds", n)
		return
	}

	if n[1] < 0 || n[1] >= game.level.width {
		// fmt.Println("out of bounds", n)
		return
	}

	obstacle := false
	if game.level.obstacles[n[0]][n[1]] {
		obstacle = true
	}
	if game.obstacles[n[0]][n[1]] {
		obstacle = true
	}

	if obstacle {
		// fmt.Println("found obstacle at", n, game.direction)
		if game.hits[game.direction][game.position[0]][game.position[1]] {
			is_loop = true
			return
		}
		game.hits[game.direction][game.position[0]][game.position[1]] = true
		switch game.direction {
		case UP:
			game.direction = RIGHT
		case RIGHT:
			game.direction = DOWN
		case DOWN:
			game.direction = LEFT
		case LEFT:
			game.direction = UP
		}
		ok = true
		return
	}

	// fmt.Println("new position", n)

	game.position = n
	ok = true
	return
}

func (level *Level) part1() int {
	game := Game{
		level: level,
	}
	game.init()
	// game.obstacles[6][3] = true
	// game.obstacles[6][29] = true
	return game.part1()
}

func (game *Game) part1() int {
	ok := true
	is_loop := false
	for ok && !is_loop {
		ok, is_loop = game.step()
	}

	count := 0
	for _, row := range game.walked {
		count += len(row)
	}
	return count
}

func (level *Level) part2() int {
	baseGame := Game{level: level}
	baseGame.init()
	baseGame.part1()

	count := 0

	for y := 0; y < level.height; y++ {
		for x := 0; x < level.width; x++ {
			// fmt.Println("game with extra obstacle at ", x, "x", y)
			if level.startPosition[0] == y && level.startPosition[1] == x {
				fmt.Println("skip because startPostion")
				continue
			}
			if !baseGame.walked[y][x] == true {
				// fmt.Println("skip because have not walked here, so adding a blocker would not change the outcome")
				continue
			}
			game := Game{
				level: level,
			}
			game.init()
			game.obstacles[y][x] = true
			ok := true
			is_loop := false
			for ok && !is_loop {
				ok, is_loop = game.step()
			}
			if is_loop {
				count++
			}
		}
	}

	return count
}

func main() {
	// level := readFile("input_demo")
	level := readFile("input")
	// fmt.Println("total", level.part1())
	fmt.Println("loops", level.part2())
}
