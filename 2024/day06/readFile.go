package main

import (
	"bufio"
	"io"
	"log"
	"os"
)

func readFile(filename string) Level {
	dir, _ := os.Getwd()
	file := dir + "/" + filename

	f, err := os.Open(file)

	if err != nil {
		panic(err)
	}

	defer f.Close()

	fileScanner := bufio.NewReader(f)

	var level Level
	var line = make(map[int]bool)

	var linenr int
	var colnr int

	for {
		if rune, _, err := fileScanner.ReadRune(); err != nil {
			if err == io.EOF {
				break
			} else {
				log.Fatal(err)
			}
		} else {
			if rune == '\n' {
				linenr++
				level.width = colnr
				colnr = 0
				level.obstacles = append(level.obstacles, line)
				line = make(map[int]bool)
				continue
			}

			if rune == '^' {
				level.startPosition = [2]int{linenr, colnr}
			} else if rune == '#' {
				line[colnr] = true
			} else {
			}
			colnr++
		}
	}

	if len(line) > 0 {
		linenr++
		level.obstacles = append(level.obstacles, line)
	}

	level.height = linenr

	return level
}
