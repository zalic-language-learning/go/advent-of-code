package main

import (
	"errors"
	"fmt"
	"strconv"

	"gitlab.com/zalic-language-learning/go/advent-of-code/util"
)

type Block struct {
	isFile bool
	fileId int
}

type Puzzle struct {
	blocks       []Block
	start_cursor int
	end_cursor   int
}

func parseInput(filename string) *Puzzle {
	puzzle := Puzzle{}
	runes := make(chan rune)

	go util.ReadFileAsRunes(filename, runes)

	free := false
	fileId := 0
	for rune := range runes {
		n, _ := strconv.Atoi(string(rune))
		for i := 0; i < n; i++ {
			var block Block
			if !free {
				block.isFile = true
				block.fileId = fileId
			}
			puzzle.blocks = append(puzzle.blocks, block)
		}
		if !free {
			fileId++
		}
		free = !free
	}

	puzzle.start_cursor = 0
	puzzle.end_cursor = len(puzzle.blocks)
	return &puzzle
}

func (puzzle *Puzzle) print() {
	for _, block := range puzzle.blocks {
		if block.isFile {
			fmt.Print(block.fileId)
		} else {
			fmt.Print(".")
		}
	}
	fmt.Print("\n")
}

func (puzzle *Puzzle) print2() {
	for i := 0; i < len(puzzle.blocks); i++ {
		block := puzzle.blocks[i]
		if block.isFile {
			fmt.Println(i, block.fileId)
		} else {
			fmt.Println(i, ".")
		}
	}
}

func (puzzle *Puzzle) getFreeBlock() (int, error) {
	// we skip index 0 because we know that it always starts with a file
	for {
		puzzle.start_cursor++
		if puzzle.start_cursor == len(puzzle.blocks) {
			return 0, errors.New("out of blocks")
		}
		block := puzzle.blocks[puzzle.start_cursor]
		if !block.isFile {
			break
		}
	}

	return puzzle.start_cursor, nil
}

func (puzzle *Puzzle) getBlockToMove() (int, error) {
	for {
		puzzle.end_cursor--
		if puzzle.end_cursor <= 0 {
			return 0, errors.New("out of blocks")
		}
		block := puzzle.blocks[puzzle.end_cursor]
		if block.isFile {
			break
		}
	}

	return puzzle.end_cursor, nil
}

func (puzzle *Puzzle) moveBlock() bool {
	freeBlockId, err := puzzle.getFreeBlock()

	if err != nil {
		// fmt.Println("could not find a free block")
		return false
	}

	fileBlockId, err := puzzle.getBlockToMove()

	if err != nil {
		// fmt.Println("could not find a file chunk to move")
		return false
	}

	if freeBlockId > fileBlockId {
		// fmt.Println("dont go the other way")
		return false
	}

	// fmt.Println(fmt.Sprintf("swap %d and %d", freeBlockId, fileBlockId), puzzle.blocks[fileBlockId], puzzle.blocks[freeBlockId])
	puzzle.blocks[freeBlockId] = puzzle.blocks[fileBlockId]
	puzzle.blocks[fileBlockId] = Block{}
	return true
}

func (puzzle *Puzzle) checksum() int {
	sum := 0
	for i := 0; i < len(puzzle.blocks); i++ {
		block := puzzle.blocks[i]
		if block.isFile {
			sum += block.fileId * i
		}
	}
	return sum
}

func (puzzle *Puzzle) findFileToMove() (fileStartIndex int, fileLength int, err error) {
	block := Block{}
	for {
		puzzle.end_cursor--
		if puzzle.end_cursor < 0 {
			err = errors.New("out of blocks")
			return
		}
		block = puzzle.blocks[puzzle.end_cursor]
		if block.isFile {
			fileId := block.fileId
			fileLength = 1

			for {
				if n := puzzle.end_cursor - 1; n >= 0 && puzzle.blocks[n].fileId == fileId {
					fileLength++
					puzzle.end_cursor--
				} else {
					break
				}
			}
			break
		}
	}

	fileStartIndex = puzzle.end_cursor

	return
}

func (puzzle *Puzzle) findNewLocationForFile(fileStartIndex int, fileLength int) (index int, err error) {
	if fileLength == 1 {
		// file is one block long, just find the first free block
		puzzle.start_cursor = 0
		return puzzle.getFreeBlock()
	}

	length := 0
	free := false
	for i := 0; i < len(puzzle.blocks); i++ {
		if puzzle.blocks[i].isFile {
			free = false
		} else if !free {
			if index > fileStartIndex {
				return 0, errors.New("could not find a free block before file")
			}
			index = i
			length = 1
			free = true
		} else {
			length++
		}
		if length == fileLength {
			return
		}
	}

	return 0, errors.New("could not find a free block")
}

func (puzzle *Puzzle) moveFiles() {
	puzzle.start_cursor = 0
	puzzle.end_cursor = len(puzzle.blocks)
	for {
		fileStartIndex, fileLength, err := puzzle.findFileToMove()
		if err != nil {
			// fmt.Println(err)
			break
		}

		newIndex, err := puzzle.findNewLocationForFile(fileStartIndex, fileLength)
		if err != nil {
			// fmt.Println(err, puzzle.blocks[fileStartIndex])
			continue
		}

		if newIndex > fileStartIndex {
			continue
		}

		for i := 0; i < fileLength; i++ {
			puzzle.blocks[newIndex+i] = puzzle.blocks[fileStartIndex+i]
			puzzle.blocks[fileStartIndex+i] = Block{}
		}
	}
}

func main() {
	// puzzle := parseInput("input_easy")
	// puzzle := parseInput("input_demo")
	puzzle := parseInput("input")

	// puzzle.print()
	// for puzzle.moveBlock() {
	// puzzle.print()
	// }
	// fmt.Println("checksum", puzzle.checksum())

	puzzle.moveFiles()
	// puzzle.print()
	fmt.Println("checksum", puzzle.checksum())

}
