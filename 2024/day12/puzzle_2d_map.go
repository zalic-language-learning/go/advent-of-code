package main

import (
	"errors"
	"fmt"
)

type Puzzle2dMap map[int]map[int]*Point

func (puzzle Puzzle2dMap) set(x, y int, value Point) {
	if puzzle[y] == nil {
		puzzle[y] = make(map[int]*Point)
	}

	puzzle[y][x] = &value
}

func (puzzle Puzzle2dMap) get(x, y int) (val *Point, err error) {
	if _, ok := puzzle[y]; !ok {
		err = errors.New(fmt.Sprintf("y %d is not set", y))
		return
	}

	if _, ok := puzzle[y][x]; !ok {
		err = errors.New(fmt.Sprintf("%dx%d is not set", x, y))
		return
	}

	val = puzzle[y][x]
	return
}

func (puzzle Puzzle2dMap) String() string {
	result := ""
	for y := 0; y < len(puzzle); y++ {
		for x := 0; x < len(puzzle[y]); x++ {
			val, err := puzzle.get(x, y)

			if err != nil {
				result += " "
				continue
			}

			result += string(val.char)

		}
		result += "\n"
	}
	return result
}
