package main

import (
	"fmt"
	"os"
	"strings"
)

type Area struct {
	char   rune
	points map[int]map[int]bool
}

func (area *Area) add(x, y int) {
	if area.points == nil {
		area.points = make(map[int]map[int]bool)
	}
	if area.points[y] == nil {
		area.points[y] = make(map[int]bool)
	}
	area.points[y][x] = true
}

type Point struct {
	char rune
	area *Area
}

func (point Point) String() string {
	return string(point.char)
}

func parseInput(filename string) Puzzle2dMap {
	input, err := os.ReadFile(filename)

	puzzle := Puzzle2dMap{}

	if err != nil {
		panic(err)
	}

	line_nr := 0
	for _, line := range strings.Split(string(input), "\n") {
		col_nr := 0
		for _, char := range line {
			puzzle.set(col_nr, line_nr, Point{char: char})
			col_nr++
		}
		line_nr++
	}
	return puzzle
}

func (puzzle Puzzle2dMap) growAreaTo(x, y int, from *Point) {
	point, err := puzzle.get(x, y)
	if err != nil {
		return
	}

	if point.area != nil {
		return
	}

	if point.char != from.char {
		return
	}

	point.area = from.area
	from.area.add(x, y)
	puzzle.growAreaFrom(x, y, point)
}

func (puzzle Puzzle2dMap) growAreaFrom(x, y int, point *Point) {
	puzzle.growAreaTo(x-1, y, point)
	puzzle.growAreaTo(x+1, y, point)
	puzzle.growAreaTo(x, y-1, point)
	puzzle.growAreaTo(x, y+1, point)
}

func (puzzle Puzzle2dMap) getAreas() []*Area {
	var areas []*Area
	for y := 0; y < len(puzzle); y++ {
		for x := 0; x < len(puzzle[y]); x++ {
			point, err := puzzle.get(x, y)
			if err != nil {
				continue
			}

			if point.area != nil {
				continue
			}

			area := Area{char: point.char}
			area.add(x, y)
			point.area = &area
			puzzle.growAreaFrom(x, y, point)
			areas = append(areas, &area)
		}
	}
	return areas
}

func (area *Area) getPrice() int {
	surface := 0
	perimiter := 0

	for y, row := range area.points {
		for x := range row {
			surface++
			if _, ok := area.points[y][x+1]; !ok {
				perimiter++
			}
			if _, ok := area.points[y][x-1]; !ok {
				perimiter++
			}
			if _, ok := area.points[y+1][x]; !ok {
				perimiter++
			}
			if _, ok := area.points[y-1][x]; !ok {
				perimiter++
			}
		}
	}
	// fmt.Println(string(area.char), "surface", surface, "perimiter", perimiter)
	return surface * perimiter
}
func (area *Area) getDiscountPrice() int {
	surface := 0
	sides := 0

	for y, row := range area.points {
		for x := range row {
			surface++
			if _, ok := area.points[y][x+1]; !ok {
				// right side
				// if the block "a" above me is of the same area - the side continues
				// unless block "b" above and to the right is of the same area
				_, a := area.points[y-1][x]
				_, b := area.points[y-1][x+1]
				if !a || b {
					sides++
				}
			}
			if _, ok := area.points[y][x-1]; !ok {
				// left side
				// if the block "a" above me is of the same area - the side continues
				// unless block "b" above and to the left is of the same area
				_, a := area.points[y-1][x]
				_, b := area.points[y-1][x-1]
				if !a || b {
					sides++
				}
			}
			if _, ok := area.points[y+1][x]; !ok {
				// left side
				// if the block "a" left of me is of the same area - the side continues
				// unless block "b" left and above is of the same area
				_, a := area.points[y][x-1]
				_, b := area.points[y+1][x-1]
				if !a || b {
					sides++
				}
			}
			if _, ok := area.points[y-1][x]; !ok {
				// bottom side
				// if the block "a" left of me is of the same area - the side continues
				// unless block "b" left and below is of the same area
				_, a := area.points[y][x-1]
				_, b := area.points[y-1][x-1]
				if !a || b {
					sides++
				}
			}
		}
	}
	// fmt.Println(string(area.char), "surface", surface, "sides", sides)
	return surface * sides
}

func main() {
	// puzzle := parseInput("input_easy")
	// puzzle := parseInput("input_demo")
	puzzle := parseInput("input")
	areas := puzzle.getAreas()
	part1 := 0
	part2 := 0
	for _, area := range areas {
		part1 += area.getPrice()
		part2 += area.getDiscountPrice()
	}
	fmt.Println("part1", part1)
	fmt.Println("part2", part2)
}
