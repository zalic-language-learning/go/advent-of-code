package main

import (
	"bufio"
	"io"
	"log"
	"os"
)

type Puzzle struct {
	rows int
	cols int
	data [][]rune
}

func readFile(filename string) Puzzle {
	dir, _ := os.Getwd()
	file := dir + "/" + filename

	f, err := os.Open(file)

	if err != nil {
		panic(err)
	}

	defer f.Close()

	fileScanner := bufio.NewReader(f)

	var result [][]rune
	var line []rune

	for {
		if rune, _, err := fileScanner.ReadRune(); err != nil {
			if err == io.EOF {
				break
			} else {
				log.Fatal(err)
			}
		} else {
			if rune == '\n' {
				if len(line) > 0 {
					result = append(result, line)
				}
				line = nil
			} else {
				line = append(line, rune)
			}
		}
	}

	if len(line) > 0 {
		result = append(result, line)
	}

	return Puzzle{
		rows: len(result),
		cols: len(result[0]),
		data: result,
	}
}
