package main

import (
	"errors"
	"fmt"
)

type MovementDirectionFn func([2]int) [2]int

var movementDirections = []MovementDirectionFn{
	// left to right
	func(c [2]int) [2]int { return [2]int{c[0] + 1, c[1]} },
	// right to left
	func(c [2]int) [2]int { return [2]int{c[0] - 1, c[1]} },
	// top to bottom
	func(c [2]int) [2]int { return [2]int{c[0], c[1] + 1} },
	// bottom to top
	func(c [2]int) [2]int { return [2]int{c[0], c[1] - 1} },
	// diagonal left down
	func(c [2]int) [2]int { return [2]int{c[0] - 1, c[1] + 1} },
	// diagonal right down
	func(c [2]int) [2]int { return [2]int{c[0] + 1, c[1] + 1} },
	// diagonal left up
	func(c [2]int) [2]int { return [2]int{c[0] - 1, c[1] - 1} },
	// diagonal right up
	func(c [2]int) [2]int { return [2]int{c[0] + 1, c[1] - 1} },
}

func (p *Puzzle) getChar(c [2]int) (rune, error) {
	if c[0] < 0 || c[1] < 0 || c[0] > p.cols-1 || c[1] > p.rows-1 {
		var r rune
		return r, errors.New(fmt.Sprintf("coords %d,%d are out of bounds", c[0], c[1]))
	}
	return p.data[c[1]][c[0]], nil
}

func (p *Puzzle) isWordAt(word string, coord [2]int, f MovementDirectionFn) bool {
	for i, char := range word {
		if i != 0 {
			// first char is starting coordinates
			// evry next char apply modifier function for the coordinates
			coord = f(coord)
		}
		if nextChar, err := p.getChar(coord); err != nil || nextChar != char {
			return false
		}
	}
	return true
}

func (p *Puzzle) wordCountAt(word string, coord [2]int) int {
	n := 0
	for _, fn := range movementDirections {
		if p.isWordAt(word, coord, fn) {
			n++
		}
	}
	return n
}

func (p *Puzzle) part1(word string) int {
	count := 0
	for y := 0; y < p.rows; y++ {
		for x := 0; x < p.cols; x++ {
			count += p.wordCountAt(word, [2]int{x, y})
		}
	}
	return count
}

func (p *Puzzle) isXAt(coord [2]int) bool {
	if char, err := p.getChar(coord); err != nil || char != 'A' {
		return false
	}

	a, err := p.getChar([2]int{coord[0] + 1, coord[1] + 1})
	if err != nil {
		return false
	}
	b, err := p.getChar([2]int{coord[0] - 1, coord[1] - 1})
	if err != nil {
		return false
	}
	c, err := p.getChar([2]int{coord[0] + 1, coord[1] - 1})
	if err != nil {
		return false
	}
	d, err := p.getChar([2]int{coord[0] - 1, coord[1] + 1})
	if err != nil {
		return false
	}

	if ((a == 'M' || b == 'M') && (a == 'S' || b == 'S') && a != b) && ((c == 'M' || d == 'M') && (c == 'S' || d == 'S') && c != d) {
		return true
	}

	return false
}

func (p *Puzzle) part2() int {
	count := 0
	for y := 1; y < p.rows-1; y++ {
		for x := 1; x < p.cols-1; x++ {
			if p.isXAt([2]int{x, y}) {
				count++
			}
		}
	}
	return count

}

func main() {
	puzzle := readFile("input")

	fmt.Println("part1", puzzle.part1("XMAS"))
	fmt.Println("part2", puzzle.part2())
}
