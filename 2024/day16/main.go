package main

import (
	"fmt"
	"os"
	"strings"
)

type Point struct {
	x       int
	y       int
	is_wall bool
	// pointer to path with the lowest score to reach this point
	path *Path
}

type Path []*PathPoint

type PathPoint struct {
	point  *Point
	facing MovementDirection
}

type Layout [][]Point

type Puzzle struct {
	start  [2]int                 // {x, y}
	end    [2]int                 // {x, y}
	layout map[int]map[int]*Point // [y][x]
}

type MovementDirection rune

const (
	Up    MovementDirection = '^'
	Right                   = '>'
	Down                    = 'v'
	Left                    = '<'
)

var MovementModifier = map[MovementDirection][2]int{
	Up:    {0, -1},
	Right: {1, 0},
	Down:  {0, 1},
	Left:  {-1, 0},
}

func (path Path) score() int {
	score := 0
	var lastpoint PathPoint
	for i, point := range path {
		score++
		newpoint := *point
		if i != 0 && lastpoint.facing != newpoint.facing {
			score += 1000
		}
		lastpoint = *point
	}
	// minus one because path starts with "start" but we are counting moves not tiles
	return score - 1
}

func parseInput(filename string) Puzzle {
	input, err := os.ReadFile(filename)

	if err != nil {
		panic(err)
	}

	puzzle := Puzzle{}
	puzzle.layout = make(map[int]map[int]*Point)

	for y, line := range strings.Split(string(input), "\n") {
		if line == "" {
			continue
		}

		puzzle.layout[y] = make(map[int]*Point)

		for x, char := range line {
			point := Point{
				x: x,
				y: y,
			}
			if char == '#' {
				point.is_wall = true
			}
			if char == 'S' {
				puzzle.start = [2]int{x, y}
			}
			if char == 'E' {
				puzzle.end = [2]int{x, y}
			}
			puzzle.layout[y][x] = &point
		}
	}

	return puzzle
}

func (puzzle *Puzzle) print() {
	height := len(puzzle.layout)
	width := len(puzzle.layout[0])
	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			if x == puzzle.start[0] && y == puzzle.start[1] {
				fmt.Print("S")
			} else if x == puzzle.end[0] && y == puzzle.end[1] {
				fmt.Print("E")
			} else if point, ok := puzzle.layout[y][x]; ok && point.is_wall {
				fmt.Print("#")
			} else {
				fmt.Print(".")
			}
		}
		fmt.Print("\n")
	}
}

func (path Path) String() string {
	result := "path("
	for i, point := range path {
		if i != 0 {
			result += ", "
		}
		result += fmt.Sprintf("%dx%d %s", point.point.x, point.point.y, string(point.facing))
	}
	return result + ")"
}

func (puzzle *Puzzle) pathfind() {
	var paths []*Path
	paths = append(paths, &Path{
		&PathPoint{
			point:  puzzle.layout[puzzle.start[1]][puzzle.start[0]],
			facing: Right,
		},
	})
	puzzle.layout[puzzle.start[1]][puzzle.start[0]].path = paths[0]

	for {
		var new_paths []*Path
		for _, path := range paths {
			p := *path
			last_point := p[len(p)-1]
			for direction, modifier := range MovementModifier {
				if last_point.point.x == puzzle.end[0] && last_point.point.y == puzzle.end[1] {
					continue
				}

				x := last_point.point.x + modifier[0]
				y := last_point.point.y + modifier[1]
				new_point := puzzle.layout[y][x]

				if new_point.is_wall {
					continue
				}

				old_path := new_point.path
				new_path := make(Path, len(p))
				copy(new_path, p)
				new_path = append(new_path, &PathPoint{
					point:  new_point,
					facing: direction,
				})

				if old_path == nil || new_path.score() < old_path.score() {
					new_paths = append(new_paths, &new_path)
					new_point.path = &new_path
				}
			}
		}

		if len(new_paths) == 0 {
			break
		}

		paths = new_paths
	}
}
func (puzzle *Puzzle) path() {

}
func (puzzle *Puzzle) getWinningPath() *Path {
	return puzzle.layout[puzzle.end[1]][puzzle.end[0]].path
}

func main() {
	// puzzle := parseInput("input_demo")
	// puzzle := parseInput("input_demo2")
	puzzle := parseInput("input")
	puzzle.print()
	puzzle.pathfind()
	path := puzzle.getWinningPath()
	fmt.Println(path)
	fmt.Println(path.score())
}
