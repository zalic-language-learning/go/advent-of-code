package main

import (
	"fmt"
	"math"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type Pos struct {
	x float64
	y float64
}

type Clawmachine struct {
	A     Pos
	B     Pos
	Prize Pos
}

func parseInput(filename string) (result []Clawmachine) {
	input, err := os.ReadFile(filename)

	if err != nil {
		panic(err)
	}

	lines := strings.Split(string(input), "\n")
	for i := 0; i < len(lines); i += 4 {
		r := regexp.MustCompile(".*X\\+([0-9]+), Y\\+([0-9]+).*")
		button_a := r.FindStringSubmatch(lines[i])
		button_b := r.FindStringSubmatch(lines[i+1])
		r = regexp.MustCompile(".*X=([0-9]+), Y=([0-9]+).*")
		prize := r.FindStringSubmatch(lines[i+2])

		if (len(button_a)) == 0 {
			panic(fmt.Sprintf("regexp failed for button A on line %s", lines[i]))
		}
		if (len(button_b)) == 0 {
			panic(fmt.Sprintf("regexp failed for button B on line %s", lines[i+1]))
		}
		if (len(prize)) == 0 {
			panic(fmt.Sprintf("regexp failed for prize on line %s", lines[i+2]))
		}

		ax, _ := strconv.ParseFloat(button_a[1], 64)
		ay, _ := strconv.ParseFloat(button_a[2], 64)
		bx, _ := strconv.ParseFloat(button_b[1], 64)
		by, _ := strconv.ParseFloat(button_b[2], 64)
		px, _ := strconv.ParseFloat(prize[1], 64)
		py, _ := strconv.ParseFloat(prize[2], 64)

		result = append(result, Clawmachine{
			A:     Pos{x: ax, y: ay},
			B:     Pos{x: bx, y: by},
			Prize: Pos{x: px, y: py},
		})

	}

	return
}

func isRound(x float64) bool {
	return math.Floor(x) == x
}

func (c *Clawmachine) getMinScore(part2 bool) (score float64) {
	// skip calculation the movement per token and presume it is cheaper to press button b as many times as possible
	// so we divide the prize.x with button_b.x and divide prize.y with button_b.y
	// math.floor the both of them
	// take the smaller number
	// foreach from that number to 1
	// in each iteration subtract i*button_b value from the prize
	// and see if the results are divizible by button A movement values

	prize_x := c.Prize.x
	prize_y := c.Prize.y
	if part2 {
		prize_x += 10000000000000
		prize_y += 10000000000000
	}

	x := math.Floor(prize_x / c.B.x)
	y := math.Floor(prize_y / c.B.y)

	// TODO: does this need to be min(x,y,100) ?
	for i := min(x, y); i > 0; i-- {
		x := i * c.B.x
		y := i * c.B.y
		xb := (prize_x - x) / c.A.x
		yb := (prize_y - y) / c.A.y
		fmt.Println(int(i), int(x), int(y))
		// TODO: xb <= 100 ?
		if isRound(xb) && isRound(yb) && xb == yb {
			fmt.Println("press button A", xb, "times and button B", i, "times")
			return (3 * xb) + i
		}
	}
	return
}

func main() {
	// puzzle := parseInput("input_demo")
	puzzle := parseInput("input")
	var part1 float64
	var part2 float64
	for i, machine := range puzzle {
		fmt.Println("clawmachine #", i)
		part1 += machine.getMinScore(false)
		part2 += machine.getMinScore(true)
	}
	fmt.Println("part1", part1)
	fmt.Println("part2", part2)
}
