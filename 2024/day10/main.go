package main

import (
	"fmt"
	"strconv"

	"gitlab.com/zalic-language-learning/go/advent-of-code/util"
)

type Map struct {
	// data[y-cord][x-cord] = height of the position
	data   [][]int
	width  int
	height int
}

type Point struct {
	x int
	y int
}

// Stringer interface... basically Point.toString method
func (p Point) String() string {
	return fmt.Sprintf("%dx%d", p.x, p.y)
}

func parseInput(filename string) Map {
	runes := make(chan rune)

	go util.ReadFileAsRunes(filename, runes)

	var result Map
	var line []int
	for rune := range runes {
		if rune == '\n' {
			if len(line) > 0 {
				result.width = len(line)
				result.data = append(result.data, line)
			}
			line = nil
		} else {
			n, _ := strconv.Atoi(string(rune))
			line = append(line, n)
		}
	}
	if len(line) > 0 {
		result.data = append(result.data, line)
	}
	result.height = len(result.data)

	return result
}

func (m *Map) isPointAtHeight(point Point, height int) bool {
	if point.x < 0 || point.y < 0 {
		return false
	}
	if point.x >= m.width {
		return false
	}
	if point.y >= m.height {
		return false
	}

	return m.data[point.y][point.x] == height
}

func pointExistInRoute(route []Point, point Point) bool {
	for _, p := range route {
		if p.x == point.x && p.y == point.y {
			return true
		}
	}
	return false
}

func (m *Map) findNewRoutes(route []Point, nextHeight int) (routes [][]Point) {
	p := route[len(route)-1]

	right := Point{x: p.x + 1, y: p.y}
	left := Point{x: p.x - 1, y: p.y}
	up := Point{x: p.x, y: p.y - 1}
	down := Point{x: p.x, y: p.y + 1}

	// clone existing route, append new point, append new point to routes
	if !pointExistInRoute(route, right) && m.isPointAtHeight(right, nextHeight) {
		routes = append(routes, append(append([]Point(nil), route...), right))
	}
	if !pointExistInRoute(route, left) && m.isPointAtHeight(left, nextHeight) {
		routes = append(routes, append(append([]Point(nil), route...), left))

	}
	if !pointExistInRoute(route, up) && m.isPointAtHeight(up, nextHeight) {
		routes = append(routes, append(append([]Point(nil), route...), up))
	}
	if !pointExistInRoute(route, down) && m.isPointAtHeight(down, nextHeight) {
		routes = append(routes, append(append([]Point(nil), route...), down))
	}

	return
}

func (m *Map) findTrailsFromPoint(start Point) [][]Point {
	var routes [][]Point
	routes = append(routes, []Point{start})

	for i := 1; i <= 9; i++ {
		var newRoutes [][]Point
		for _, route := range routes {
			tmp := m.findNewRoutes(route, i)
			if len(tmp) > 0 {
				newRoutes = append(newRoutes, tmp...)
			}

		}
		routes = newRoutes
	}

	return routes
}

func (m *Map) findTrailsScore(routes [][]Point) int {
	uniqueEndings := make(map[string]bool)
	for _, route := range routes {
		p := route[len(route)-1]
		uniqueEndings[fmt.Sprint(p)] = true
	}
	return len(uniqueEndings)
}

func (m *Map) findStartingPoints() (starts []Point) {
	for y := 0; y < m.height; y++ {
		for x := 0; x < m.width; x++ {
			if m.data[y][x] == 0 {
				starts = append(starts, Point{x: x, y: y})
			}
		}
	}
	return
}

func (m *Map) part1() (result int) {
	for _, start := range m.findStartingPoints() {
		result += m.findTrailsScore(m.findTrailsFromPoint(start))
	}
	return
}

func (m *Map) part2() (result int) {
	for _, start := range m.findStartingPoints() {
		result += len(m.findTrailsFromPoint(start))
	}
	return
}

func (m *Map) printRoutes(routes [][]Point) {
	for y := 0; y < m.height; y++ {
		for x := 0; x < m.width; x++ {
			point := Point{x: x, y: y}
			yes := false
			for _, route := range routes {
				if pointExistInRoute(route, point) {
					yes = true
					break
				}
			}
			if yes {
				fmt.Print(m.data[y][x])
			} else {
				fmt.Print(".")
			}
		}
		fmt.Print("\n")
	}
}

func main() {
	// m := parseInput("input_demo")
	m := parseInput("input")
	fmt.Println("part1", m.part1())
	fmt.Println("part2", m.part2())
}
