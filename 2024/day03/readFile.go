package main

import (
	"bufio"
	"io"
	"log"
	"os"
)

func readFile(filename string) []rune {
	dir, _ := os.Getwd()
	file := dir + "/" + filename

	f, err := os.Open(file)

	if err != nil {
		panic(err)
	}

	defer f.Close()

	fileScanner := bufio.NewReader(f)

	var result []rune

	for {
		if rune, _, err := fileScanner.ReadRune(); err != nil {
			if err == io.EOF {
				break
			} else {
				log.Fatal(err)
			}
		} else {
			result = append(result, rune)
		}
	}

	return result
}
