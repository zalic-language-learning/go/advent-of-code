package main

import (
	"fmt"
	"strconv"
	"unicode"
)

func parseIntput(filename string) {
	chars := readFile(filename)

	var result [][2]string
	i := 0
	var doNext bool = true
	for i < len(chars) {
		if string(chars[i:i+7]) == "don't()" {
			doNext = false
			i = i + 7
		} else if string(chars[i:i+4]) == "do()" {
			doNext = true
			i = i + 4
		} else if doNext && string(chars[i:i+4]) == "mul(" {
			var arg []rune
			var args [][]rune
			i = i + 4
			for {
				char := chars[i]
				if char == ',' {
					args = append(args, arg)
					arg = nil
				} else if char == ')' {
					args = append(args, arg)
					// if we have found ecatly 2 arguments
					// and neither is empty
					// convert them to string and append to result
					if len(args) == 2 && len(args[0]) > 0 && len(args[1]) > 0 {
						result = append(result, [2]string{string(args[0]), string(args[1])})
					}
					break
				} else if unicode.IsDigit(char) {
					arg = append(arg, char)
				} else {
					// invalid character
					break
				}
				i++
			}
		} else {
			i++
		}
	}

	total := 0
	for _, pair := range result {
		a, _ := strconv.Atoi(pair[0])
		b, _ := strconv.Atoi(pair[1])
		total += a * b
	}
	fmt.Println(total)
}

func main() {
	// parseIntput("input_demo")
	// parseIntput("input_demo2")
	parseIntput("input")
}
