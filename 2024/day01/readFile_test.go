package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseLine(t *testing.T) {
	cases := map[string][2]int{
		"1   2":     {1, 2},
		"123   456": {123, 456},
		"1, 2":      {0, 0},
	}

	for input, want := range cases {
		result, _ := parseLine(input)
		assert.Equal(t, want, result, "Failed to parse line.")
	}
}
