package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
)

const SEPARATOR = "   "

func parseLine(input string) ([2]int, error) {
	var result [2]int
	var err error

	numbers := strings.Split(input, SEPARATOR)

	if len(numbers) != 2 {
		return result, errors.New(fmt.Sprintf("line \"%s\" does not seem to contain separator \"%s\"", input, SEPARATOR))
	}

	result[0], err = strconv.Atoi(numbers[0])
	if err != nil {
		return result, errors.New(fmt.Sprintf("\"%s\" a does not seem to be a valid number", numbers[0]))
	}

	result[1], err = strconv.Atoi(numbers[1])
	if err != nil {
		return result, errors.New(fmt.Sprintf("\"%s\" b does not seem to be a valid number", numbers[1]))
	}

	return result, nil
}

func readFile(filename string) [2][]int {
	dir, _ := os.Getwd()
	file := dir + "/" + filename

	f, err := os.Open(file)

	if err != nil {
		panic(err)
	}

	defer f.Close()

	fileScanner := bufio.NewScanner(f)
	fileScanner.Split(bufio.ScanLines)

	var result [2][]int

	for fileScanner.Scan() {
		line := fileScanner.Text()
		numbers, err := parseLine(line)

		if err != nil {
			fmt.Println(err)
		}

		if len(numbers) == 2 {
			result[0] = append(result[0], numbers[0])
			result[1] = append(result[1], numbers[1])
		}
	}

	return result
}
