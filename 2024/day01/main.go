package main

import (
	"fmt"
	"math"
	"slices"
)

func findCount(haystack []int, key int) int {
	count := 0
	for _, item := range haystack {
		if item == key {
			count++
		}
	}

	return count
}

func part1(filename string) int {
	input := readFile(filename)
	slices.Sort(input[0])
	slices.Sort(input[1])

	var dif int
	for i := 0; i < len(input[0]); i++ {
		line_diff := int(math.Abs(float64(input[1][i] - input[0][i])))
		dif += line_diff
	}

	return dif
}

func part2(filename string) int {
	input := readFile(filename)

	similarity_score := 0
	for i := 0; i < len(input[0]); i++ {
		n := input[0][i]
		c := findCount(input[1], n)
		similarity_score += n * c
	}

	return similarity_score
}

func main() {
	fmt.Println("demo:")
	fmt.Println(fmt.Sprintf("\tpart1: %d", part1("input_demo")))
	fmt.Println(fmt.Sprintf("\tpart2: %d", part2("input_demo")))
	fmt.Println("puzzle:")
	fmt.Println(fmt.Sprintf("\tpart1: %d", part1("input")))
	fmt.Println(fmt.Sprintf("\tpart2: %d", part2("input")))
}
