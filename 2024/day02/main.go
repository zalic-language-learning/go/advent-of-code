package main

import (
	"fmt"
	"strconv"
	"strings"
)

type Direction int

const (
	Increasing Direction = 1
	Decreasing           = -1
	Unkown               = 0
)

type Report struct {
	levels    []int
	direction Direction
}

func (r *Report) isOkPair(a, b int) bool {
	if r.direction == Decreasing {
		return a > b && a-b <= 3
	}
	return b > a && b-a <= 3
}

func (r *Report) isSafe() bool {
	for i := 1; i < len(r.levels); i++ {
		n0 := r.levels[i-1]
		n1 := r.levels[i]

		if n0 == n1 {
			return false
		}

		if i == 1 {
			if n0 > n1 {
				r.direction = Decreasing
			}
			if n0 < n1 {
				r.direction = Increasing
			}
		}

		if !r.isOkPair(n0, n1) {
			return false
		}
	}

	return true
}

func parseInput(filename string, rows chan<- Report) {
	lines := make(chan string)

	go readFileToChannel(filename, lines)

	for line := range lines {
		report := Report{}

		for _, n := range strings.Split(line, " ") {
			if nr, err := strconv.Atoi(n); err == nil {
				report.levels = append(report.levels, nr)
			}
		}

		rows <- report
	}
	close(rows)
}

func main() {
	rows := make(chan Report)
	go parseInput("input", rows)
	count := 0
	for row := range rows {
		if row.isSafe() {
			count++
		}
	}
	fmt.Println(count)
}
