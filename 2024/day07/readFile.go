package main

import (
	"bufio"
	"os"
	"strconv"
	"strings"
)

func readFile(filename string) []Equation {
	dir, _ := os.Getwd()
	file := dir + "/" + filename

	f, err := os.Open(file)

	if err != nil {
		panic(err)
	}

	defer f.Close()

	fileScanner := bufio.NewScanner(f)
	fileScanner.Split(bufio.ScanLines)

	var result []Equation

	for fileScanner.Scan() {
		line := fileScanner.Text()
		a := strings.Split(line, ": ")
		if len(a) < 2 {
			continue
		}
		b := strings.Split(a[1], " ")
		if len(a) < 2 {
			continue
		}

		s, _ := strconv.Atoi(a[0])
		var v []int
		for _, n := range b {
			if n, err := strconv.Atoi(n); err == nil {
				v = append(v, n)
			}
		}

		result = append(result, Equation{result: s, args: v})
	}
	return result
}
