package main

import (
	"fmt"
	"strconv"
)

type Equation struct {
	result int
	args   []int
	count  int
}

func (eq *Equation) part1(part2 bool) int {
	stack := []int{eq.args[0]}
	for i := 1; i < len(eq.args); i++ {
		var a = eq.args[i]
		var new_stack []int
		for _, n := range stack {
			if r := n * a; r <= eq.result {
				new_stack = append(new_stack, r)
			}
			if r := n + a; r <= eq.result {
				new_stack = append(new_stack, r)
			}
			if part2 {
				t := fmt.Sprintf("%d%d", n, a)
				if num, err := strconv.Atoi(t); err == nil && num <= eq.result {
					new_stack = append(new_stack, num)
				}

			}
		}
		stack = new_stack
	}

	c := 0
	for _, s := range stack {
		if s == eq.result {
			c++
		}
	}

	eq.count = c
	return c
}

func main() {
	// input := readFile("input_demo")
	input := readFile("input")

	part1 := 0
	part2 := 0
	for _, eq := range input {
		if eq.part1(false); eq.count > 0 {
			part1 += eq.result
		}
		if eq.part1(true); eq.count > 0 {
			part2 += eq.result
		}
	}
	fmt.Println("part1", part1)
	fmt.Println("part2", part2)
}
