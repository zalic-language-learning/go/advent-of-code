package main

import (
	"fmt"

	"gitlab.com/zalic-language-learning/go/advent-of-code/util"
)

type Point struct {
	x int
	y int
}

type Puzzle struct {
	antennas map[rune][]Point
	// dont use Point for antinodes because easier to deduplicate this way
	antinodes []map[int]bool
	height    int
	width     int
}

func parseInput(filename string) Puzzle {
	runes := make(chan rune)

	go util.ReadFileAsRunes(filename, runes)

	puzzle := Puzzle{antennas: make(map[rune][]Point)}

	col := 0
	line := 0
	for rune := range runes {
		if rune == '\n' {
			puzzle.width = col
			line++
			col = 0
			continue
		}

		if rune == '.' {
			col++
			continue
		}

		puzzle.antennas[rune] = append(
			puzzle.antennas[rune],
			Point{x: col, y: line},
		)

		col++
	}

	puzzle.height = line

	for i := 0; i < puzzle.height; i++ {
		puzzle.antinodes = append(puzzle.antinodes, make(map[int]bool))
	}

	return puzzle
}

func (puzzle *Puzzle) calculateAntinodes(part2 bool) {
	for _, cords := range puzzle.antennas {
		if len(cords) < 2 {
			continue
		}
		for a := 0; a < len(cords); a++ {
			for b := 0; b < len(cords); b++ {
				if a == b {
					continue
				}
				diff_x := cords[a].x - cords[b].x
				diff_y := cords[a].y - cords[b].y
				point := cords[a]
				if part2 {
					puzzle.antinodes[point.y][point.x] = true
				}
				doMore := true
				for doMore {
					point.x = point.x + diff_x
					point.y = point.y + diff_y
					doMore = part2
					if point.x >= 0 && point.x < puzzle.width && point.y >= 0 && point.y < puzzle.height {
						puzzle.antinodes[point.y][point.x] = true
					} else {
						doMore = false
					}
				}
			}
		}
	}
}

func (puzzle *Puzzle) countAntinodes() int {
	count := 0
	for y := 0; y < puzzle.height; y++ {
		count += len(puzzle.antinodes[y])
	}
	return count
}

func (puzzle *Puzzle) print() {
	for y := 0; y < puzzle.height; y++ {
		for x := 0; x < puzzle.width; x++ {
			out := '.'
			if puzzle.antinodes[y][x] {
				out = '#'
			}
			for rune, cords := range puzzle.antennas {
				for _, cord := range cords {
					if cord.x == x && cord.y == y {
						out = rune
					}
				}
			}
			fmt.Print(string(out))
		}
		fmt.Print("\n")
	}
}

func main() {
	// puzzle := parseInput("input_easy")
	// puzzle := parseInput("input_demo")
	puzzle := parseInput("input")
	puzzle.calculateAntinodes(false)
	puzzle.print()
	fmt.Println("part1:", puzzle.countAntinodes())
	puzzle.calculateAntinodes(true)
	puzzle.print()
	fmt.Println("part2:", puzzle.countAntinodes())
}
