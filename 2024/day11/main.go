package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

type List map[int]int

func (m List) add(key, value int) {
	if _, ok := m[key]; ok {
		m[key] += value
	} else {
		m[key] = value
	}
}

func parseInput(filename string) List {
	input, err := os.ReadFile(filename)

	list := List{}

	if err != nil {
		panic(err)
	}

	for _, line := range strings.Split(string(input), "\n") {
		for _, chunk := range strings.Split(line, " ") {
			if chunk == "" {
				continue
			}
			if n, err := strconv.Atoi(chunk); err == nil {
				list.add(n, 1)
			} else {
				fmt.Println(err)
			}
		}
	}

	return list
}

func (list List) blink() List {
	newList := List{}

	for nr, count := range list {
		// If the stone is engraved with the number 0,
		// it is replaced by a stone engraved with the number 1.
		if nr == 0 {
			newList.add(1, count)
			continue
		}

		// If the stone is engraved with a number that has an even number of digits, it is replaced by two stones.
		// The left half of the digits are engraved on the new left stone,i
		// and the right half of the digits are engraved on the new right stone.
		// (The new numbers don't keep extra leading zeroes: 1000 would become stones 10 and 0.)
		s := fmt.Sprintf("%d", nr)
		if len(s)%2 == 0 {
			p := len(s) / 2
			left, _ := strconv.Atoi(s[:p])
			right, _ := strconv.Atoi(s[p:])
			newList.add(left, count)
			newList.add(right, count)
			continue
		}

		// If none of the other rules apply
		// the stone is replaced by a new stone;
		// the old stone's number multiplied by 2024 is engraved on the new stone.
		nv := nr * 2024
		newList.add(nv, count)
	}

	return newList
}

func (list List) stoneCount() (total int) {
	for _, count := range list {
		total += count
	}
	return total
}

func main() {
	// list := parseInput("input_demo")
	list := parseInput("input")

	for i := 0; i < 25; i++ {
		list = list.blink()
	}
	fmt.Println("part1", list.stoneCount())

	for i := 0; i < 50; i++ {
		list = list.blink()
	}
	fmt.Println("part2", list.stoneCount())
}
