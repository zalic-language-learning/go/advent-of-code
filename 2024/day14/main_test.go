package main

import (
	"testing"
)

func TestDemo(t *testing.T) {
	puzzle := Puzzle{
		robots: parseInput("input_demo"),
	}

	if len(puzzle.robots) != 12 {
		t.Fatalf(`Expected 12 robot got %d`, len(puzzle.robots))
	}
}
func TestMovement(t *testing.T) {
	robot := Robot{x: 2, y: 4, vx: 2, vy: -3}
	puzzle := Puzzle{
		width:  11,
		height: 7,
		robots: []*Robot{&robot},
	}

	var days = [][2]int{{2, 4}, {4, 1}, {6, 5}, {8, 2}, {10, 6}}

	for i, expect := range days {
		if robot.x != expect[0] || robot.y != expect[1] {
			t.Fatalf("frame %d - Expected robot to be at %dx%d it is instead at %dx%d", i, expect[0], expect[1], robot.x, robot.y)
		}
		puzzle.move()

		// if i == 0 {
		// 	fmt.Println("Initial state:")
		// } else {
		// 	fmt.Println(fmt.Sprintf("After %d seconds:", i))
		// }
		// fmt.Println(puzzle.String())
	}

}
