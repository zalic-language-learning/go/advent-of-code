package main

import (
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type Robot struct {
	x  int
	y  int
	vx int
	vy int
}

type Puzzle struct {
	width  int
	height int
	robots []*Robot
}

func (robot Robot) String() string {
	return fmt.Sprintf("%dx%d [%d, %d]", robot.x, robot.y, robot.vx, robot.vy)
}

func parseInput(filename string) (result []*Robot) {
	input, err := os.ReadFile(filename)

	if err != nil {
		panic(err)
	}

	for _, line := range strings.Split(string(input), "\n") {
		if line == "" {
			continue
		}

		r := regexp.MustCompile("^p=([0-9]+),([0-9]+) v=([0-9-]+),([0-9-]+)")
		m := r.FindStringSubmatch(line)

		if (len(m)) == 0 {
			panic(fmt.Sprintf("failed to match line %s", line))
		}

		x, _ := strconv.Atoi(m[1])
		y, _ := strconv.Atoi(m[2])
		vx, _ := strconv.Atoi(m[3])
		vy, _ := strconv.Atoi(m[4])

		result = append(result, &Robot{x, y, vx, vy})
	}

	return
}

func (puzzle *Puzzle) move() {
	for _, robot := range puzzle.robots {
		nx := robot.x + robot.vx
		ny := robot.y + robot.vy

		if nx >= puzzle.width {
			nx -= puzzle.width
		}
		if nx < 0 {
			nx += puzzle.width
		}
		if ny >= puzzle.height {
			ny -= puzzle.height
		}
		if ny < 0 {
			ny += puzzle.height
		}

		robot.x = nx
		robot.y = ny
	}
}

func (puzzle *Puzzle) getSafetyFactor() int {
	q1 := 0
	q2 := 0
	q3 := 0
	q4 := 0

	hx := puzzle.width / 2
	hy := puzzle.height / 2
	for y := 0; y < puzzle.height; y++ {
		for x := 0; x < puzzle.width; x++ {
			// note that middle column and row are ignored
			if y > hy {
				if x > hx {
					q4 += puzzle.getNumberOfRobots(x, y)
				} else if x < hx {
					q3 += puzzle.getNumberOfRobots(x, y)
				}
			} else if y < hy {
				if x > hx {
					q2 += puzzle.getNumberOfRobots(x, y)
				} else if x < hx {
					q1 += puzzle.getNumberOfRobots(x, y)
				}
			}
		}
	}

	return q1 * q2 * q3 * q4
}

func (puzzle *Puzzle) getNumberOfRobots(x, y int) int {
	total := 0
	for _, robot := range puzzle.robots {
		if robot.x == x && robot.y == y {
			total++
		}
	}
	return total
}

func (puzzle Puzzle) String() string {
	result := ""
	for y := 0; y < puzzle.height; y++ {
		if y != 0 {
			result += "\n"
		}
		for x := 0; x < puzzle.width; x++ {
			c := puzzle.getNumberOfRobots(x, y)
			if c == 0 {
				result += "."
			} else {
				result += strconv.Itoa(c)
			}
		}
	}
	return result
}

func (puzzle Puzzle) printSquares() string {
	result := ""
	for y := 0; y < puzzle.height; y++ {
		if y != 0 {
			result += "\n"
		}
		for x := 0; x < puzzle.width; x++ {
			c := puzzle.getNumberOfRobots(x, y)
			if c == 0 {
				result += " "
			} else {
				result += "■"
			}
		}
	}
	return result
}

func (puzzle *Puzzle) hasTriangle() bool {
	// check if the puzzle outut contains more than 1 robot in a triangle pattern
	//   ■
	//  ■■■
	// ■■■■■
	// note: got lucky that the tree was filled,
	//       if it would have only an outline,
	//       would have gotten a lot more non-christmas-tree hits
	//       with outline would have needed to check 4 lines
	//       to get christmast tree with a first hit
	//    ■
	//   ■ ■
	//  ■   ■
	// ■     ■
	for y := 0; y < puzzle.height-10; y++ {
		for x := 4; x < puzzle.width-4; x++ {
			if puzzle.getNumberOfRobots(x, y) > 0 &&
				puzzle.getNumberOfRobots(x-1, y+1) > 0 &&
				puzzle.getNumberOfRobots(x, y+1) > 0 &&
				puzzle.getNumberOfRobots(x+1, y+1) > 0 &&
				puzzle.getNumberOfRobots(x-2, y+2) > 0 &&
				puzzle.getNumberOfRobots(x-1, y+2) > 0 &&
				puzzle.getNumberOfRobots(x, y+2) > 0 &&
				puzzle.getNumberOfRobots(x+1, y+2) > 0 &&
				puzzle.getNumberOfRobots(x+2, y+2) > 0 {
				return true
			}
		}
	}
	return false
}

func main() {
	// puzzle := Puzzle{
	// 	width:  11,
	// 	height: 7,
	// 	robots: parseInput("input_demo"),
	// }
	puzzle := Puzzle{
		width:  101,
		height: 103,
		robots: parseInput("input"),
	}

	// part 1
	// for i := 0; i < 100; i++ {
	// 	puzzle.move()
	// }
	// fmt.Println(puzzle.getSafetyFactor())

	// part 2 is iteactive, automagic finds tirangles,
	// hit enter until you find a christmas tree
	// spoilers: it's the first result
	i := 0
	for {
		if puzzle.hasTriangle() {
			fmt.Println(fmt.Sprintf("frame %d", i))
			fmt.Println(puzzle.printSquares())
			// break
			fmt.Scanln()
		} else {
			fmt.Print(fmt.Sprintf("frame %d\r", i))
		}
		i++
		puzzle.move()
	}
}
