package main

import (
	"fmt"
	"os"
	"strings"
)

type MapMarker rune

var debug bool

const (
	Wall     MapMarker = '#'
	Barrel             = 'O'
	Robot              = '@'
	BoxLeft            = '['
	BoxRight           = ']'
)

type MovementDirection rune

const (
	Up    MovementDirection = '^'
	Right                   = '>'
	Down                    = 'v'
	Left                    = '<'
)

var MovementModifier = map[MovementDirection][2]int{
	Up:    {0, -1},
	Right: {1, 0},
	Down:  {0, 1},
	Left:  {-1, 0},
}

type Puzzle struct {
	step      int
	width     int
	height    int
	robot     [2]int // {x, y}
	area      map[int]map[int]MapMarker
	movements []MovementDirection
	part2     bool
}

func parseInput(filename string, part2 bool) Puzzle {
	input, err := os.ReadFile(filename)

	if err != nil {
		panic(err)
	}

	puzzle := Puzzle{part2: part2}
	puzzle.area = make(map[int]map[int]MapMarker)

	for y, line := range strings.Split(string(input), "\n") {
		if line == "" {
			continue
		}

		is_map := rune(line[0]) == rune(Wall)
		for x, char := range line {
			if is_map {
				if puzzle.area[y] == nil {
					puzzle.area[y] = make(map[int]MapMarker)
				}
				c := MapMarker(char)
				if c == Robot {
					if part2 {
						puzzle.robot = [2]int{x + x, y}
					} else {
						puzzle.robot = [2]int{x, y}
					}
				}
				if c == Wall {
					if part2 {
						puzzle.area[y][x+x] = c
						puzzle.area[y][x+x+1] = c
					} else {
						puzzle.area[y][x] = c
					}
				}
				if c == Barrel {
					if part2 {
						puzzle.area[y][x+x] = BoxLeft
						puzzle.area[y][x+x+1] = BoxRight
					} else {
						puzzle.area[y][x] = c
					}
				}
			} else {
				puzzle.movements = append(puzzle.movements, MovementDirection(char))
			}
		}
	}

	// because of the border, we now that each line has at least one char
	// and first and last line have a (Wall) char for every col
	puzzle.height = len(puzzle.area)
	puzzle.width = len(puzzle.area[0])

	return puzzle
}

func (puzzle *Puzzle) getAreaPrintout() string {
	result := ""
	for y := 0; y < puzzle.height; y++ {
		for x := 0; x < puzzle.width; x++ {
			if x == puzzle.robot[0] && y == puzzle.robot[1] {
				result += "@"
			} else if char, ok := puzzle.area[y][x]; ok {
				result += string(char)
			} else {
				result += "."
			}
		}
		result += "\n"
	}
	return result
}

func (puzzle *Puzzle) movement(from [2]int, direction MovementDirection, dry bool) bool {
	next := [2]int{from[0] + MovementModifier[direction][0], from[1] + MovementModifier[direction][1]}

	p, ok := puzzle.area[next[1]][next[0]]
	is_robot := from[0] == puzzle.robot[0] && from[1] == puzzle.robot[1]

	if p == Wall {
		if debug {
			fmt.Println("Hit a wall at", next[0], next[1])
		}
		return false
	}

	if p == Barrel || ((direction == Left || direction == Right) && (p == BoxLeft || p == BoxRight)) {
		if debug {
			fmt.Println("Found barrel at", next[0], next[1])
		}

		isGood := puzzle.movement(next, direction, dry)

		if isGood && !dry {
			if is_robot {
				puzzle.robot = next
				delete(puzzle.area[next[1]], next[0])
			} else {
				puzzle.area[next[1]][next[0]] = puzzle.area[from[1]][from[0]]
			}
		}

		return isGood
	}

	if p == BoxLeft || p == BoxRight {
		modifier := 1
		if p == BoxRight {
			modifier = -1
		}

		next_partner := [2]int{next[0] + modifier, next[1]}

		isGood := puzzle.movement(next, direction, true)
		isGoodPartner := puzzle.movement(next_partner, direction, true)

		if isGood && isGoodPartner && !dry {

			// execute again now without forcing dry-run
			puzzle.movement(next, direction, dry)
			puzzle.movement(next_partner, direction, dry)

			if is_robot {
				puzzle.robot = next
				delete(puzzle.area[next[1]], next[0])
				delete(puzzle.area[next_partner[1]], next_partner[0])
			} else {
				puzzle.area[next[1]][next[0]] = puzzle.area[from[1]][from[0]]
				delete(puzzle.area[next_partner[1]], next_partner[0])
			}
		}

		return isGood && isGoodPartner
	}

	if !ok {
		if debug {
			fmt.Println("Found empty slot at", next[0], next[1])
		}

		if dry {
			return true
		}

		if is_robot {
			puzzle.robot = next
			delete(puzzle.area[next[1]], next[0])
		} else {
			puzzle.area[next[1]][next[0]] = puzzle.area[from[1]][from[0]]
		}

		return true
	}

	panic("unhandled movement logic")
}

func (puzzle *Puzzle) move() {
	if len(puzzle.movements) == 0 {
		fmt.Println("out of moves")
		return
	}

	move := puzzle.movements[puzzle.step]
	puzzle.step++

	if debug {
		fmt.Println("Move", string(move), "from", puzzle.robot)
	}

	puzzle.movement(puzzle.robot, move, false)
}

func (puzzle *Puzzle) getSum() int {
	result := 0
	for y := 0; y < puzzle.height; y++ {
		for x := 0; x < puzzle.width; x++ {
			if puzzle.area[y][x] == Barrel || puzzle.area[y][x] == BoxLeft {
				result += 100*y + x
			}
		}
	}
	return result
}

func main() {
	debug = false
	part2 := true

	// puzzle := parseInput("input_easy", part2)
	// puzzle := parseInput("input_box", part2)
	// puzzle := parseInput("input_demo", part2)
	puzzle := parseInput("input", part2)

	// fmt.Println(puzzle.getAreaPrintout())
	for i := 0; i < len(puzzle.movements); i++ {
		puzzle.move()
		// fmt.Println(puzzle.getAreaPrintout())
	}
	fmt.Println(puzzle.getSum())
}
