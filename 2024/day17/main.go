package main

import (
	"fmt"
	"math"
	"os"
	"reflect"
	"strconv"
	"strings"
)

type Puzzle struct {
	A       int
	B       int
	C       int
	Program []int
	pointer int
}

func parseData(filename string) *Puzzle {
	input, err := os.ReadFile(filename)

	if err != nil {
		panic(err)
	}

	puzzle := Puzzle{}
	for _, line := range strings.Split(string(input), "\n") {
		x := strings.Split(line, ": ")
		if x[0] == "Register A" {
			v, _ := strconv.Atoi(x[1])
			puzzle.A = v
		}
		if x[0] == "Register B" {
			v, _ := strconv.Atoi(x[1])
			puzzle.B = v
		}
		if x[0] == "Register C" {
			v, _ := strconv.Atoi(x[1])
			puzzle.C = v
		}
		if x[0] == "Program" {
			for _, nr := range strings.Split(x[1], ",") {
				v, err := strconv.Atoi(nr)
				if err == nil {
					puzzle.Program = append(puzzle.Program, v)
				}
			}

		}
	}

	return &puzzle
}

func (puzzle *Puzzle) cycle() (output []int) {
	for {
		opcode := puzzle.Program[puzzle.pointer]
		operand := puzzle.Program[puzzle.pointer+1]

		combo := operand
		if operand == 4 {
			combo = puzzle.A
		}
		if operand == 5 {
			combo = puzzle.B
		}
		if operand == 6 {
			combo = puzzle.C
		}

		// The adv instruction (opcode 0) performs division.
		// The numerator is the value in the A register.
		// The denominator is found by raising 2 to the power of the instruction's combo operand.
		// (So, an operand of 2 would divide A by 4 (2^2); an operand of 5 would divide A by 2^B.)
		// The result of the division operation is truncated to an integer and then written to the A register.
		if opcode == 0 {
			numerator := puzzle.A
			denominator := int(math.Pow(2, float64(combo)))
			result := numerator / denominator
			puzzle.A = result
		}

		// The bxl instruction (opcode 1) calculates the bitwise XOR of register B and the instruction's literal operand,
		// then stores the result in register B.
		if opcode == 1 {
			result := puzzle.B ^ operand
			puzzle.B = result
		}

		// The bst instruction (opcode 2) calculates the value of its combo operand modulo 8
		// (thereby keeping only its lowest 3 bits),
		// then writes that value to the B register.
		if opcode == 2 {
			result := combo % 8
			puzzle.B = result
		}

		// The jnz instruction (opcode 3) does nothing if the A register is 0.
		// However, if the A register is not zero,
		// it jumps by setting the instruction pointer to the value of its literal operand;
		// if this instruction jumps,
		// the instruction pointer is not increased by 2 after this instruction.
		if opcode == 3 {
			if puzzle.A != 0 {
				puzzle.pointer = operand - 2
			}
		}

		// The bxc instruction (opcode 4) calculates the bitwise XOR of register B and register C,
		// then stores the result in register B.
		if opcode == 4 {
			result := puzzle.B ^ puzzle.C
			puzzle.B = result
		}

		// The out instruction (opcode 5) calculates the value of its combo operand modulo 8,
		// then outputs that value.
		if opcode == 5 {
			result := combo % 8
			output = append(output, result)
		}

		// The bdv instruction (opcode 6) performs division.
		// The numerator is the value in the A register.
		// The denominator is found by raising 2 to the power of the instruction's combo operand.
		// (So, an operand of 2 would divide A by 4 (2^2); an operand of 5 would divide A by 2^B.)
		// The result of the division operation is truncated to an integer and then written to the B register.
		if opcode == 6 {
			numerator := puzzle.A
			denominator := int(math.Pow(2, float64(combo)))
			result := numerator / denominator
			puzzle.B = result
		}

		// The bdv instruction (opcode 7) performs division.
		// The numerator is the value in the A register.
		// The denominator is found by raising 2 to the power of the instruction's combo operand.
		// (So, an operand of 2 would divide A by 4 (2^2); an operand of 5 would divide A by 2^B.)
		// The result of the division operation is truncated to an integer and then written to the C register.
		if opcode == 7 {
			numerator := puzzle.A
			denominator := int(math.Pow(2, float64(combo)))
			result := numerator / denominator
			puzzle.C = result
		}

		puzzle.pointer += 2

		if puzzle.pointer >= len(puzzle.Program) {
			return
		}
	}
}

func main() {
	// puzzle := parseData("input_demo")
	puzzle := parseData("input")

	fmt.Println("A", puzzle.A)
	fmt.Println("B", puzzle.B)
	fmt.Println("C", puzzle.C)
	output := ""
	for i, key := range puzzle.cycle() {
		if i != 0 {
			output += ","
		}
		output += strconv.Itoa(key)
	}
	fmt.Println("output", output)

	// 333m6.710s
	// around 20 00 000 000

	found := false
	i := 0
	for !found {
		i++
		puzzle.A = i
		puzzle.B = 0
		puzzle.C = 0
		puzzle.pointer = 0
		fmt.Print(i, "\r")
		output := puzzle.cycle()
		if reflect.DeepEqual(output, puzzle.Program) {
			fmt.Println("part2", i)
			found = true
		}
	}
}
