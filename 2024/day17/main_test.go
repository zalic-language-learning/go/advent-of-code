package main

import (
	"reflect"
	"testing"
)

func TestInvalidChar(t *testing.T) {
	var puzzle Puzzle
	var output []int
	var want []int

	puzzle = Puzzle{C: 9, Program: []int{2, 6}}
	output = puzzle.cycle()
	if puzzle.B != 1 {
		t.Fatalf(`If register C contains 9, the program 2,6 would set register B to 1 - but got %d`, puzzle.B)
	}

	puzzle = Puzzle{A: 10, Program: []int{5, 0, 5, 1, 5, 4}}
	want = []int{0, 1, 2}
	output = puzzle.cycle()
	if !reflect.DeepEqual(output, want) {
		t.Fatalf(`If register A contains 10, the program 5,0,5,1,5,4 would output 0,1,2 - but got %d`, output)
	}

	puzzle = Puzzle{A: 2024, Program: []int{0, 1, 5, 4, 3, 0}}
	want = []int{4, 2, 5, 6, 7, 7, 7, 7, 3, 1, 0}
	output = puzzle.cycle()
	if !reflect.DeepEqual(output, want) || puzzle.A != 0 {
		t.Fatalf(`If register A contains 2024, the program 0,1,5,4,3,0 would output 4,2,5,6,7,7,7,7,3,1,0 and leave 0 in register A. - but got %d, and registry A is %d`, output, puzzle.A)
	}

	puzzle = Puzzle{B: 29, Program: []int{1, 7}}
	output = puzzle.cycle()
	if puzzle.B != 26 {
		t.Fatalf(`If register B contains 29, the program 1,7 would set register B to 26. - but got %d`, puzzle.B)
	}

	puzzle = Puzzle{B: 2024, C: 43690, Program: []int{4, 0}}
	output = puzzle.cycle()
	if puzzle.B != 44354 {
		t.Fatalf(`If register B contains 2024 and register C contains 43690, the program 4,0 would set register B to 44354 - but got %d`, puzzle.B)
	}
}
