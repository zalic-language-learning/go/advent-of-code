package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Puzzle struct {
	width  int
	height int
	bytes  [][2]int // x,y
}

func parseInput(filename string) *Puzzle {
	input, err := os.ReadFile(filename)

	if err != nil {
		panic(err)
	}

	puzzle := Puzzle{}

	for _, line := range strings.Split(string(input), "\n") {
		t := strings.Split(line, ",")
		if len(t) != 2 {
			continue
		}

		x, _ := strconv.Atoi(t[0])
		y, _ := strconv.Atoi(t[1])

		puzzle.bytes = append(puzzle.bytes, [2]int{x, y})
	}
	return &puzzle
}

func (puzzle *Puzzle) getMapAfterNBytes(bytes int) map[int]map[int]bool {
	m := make(map[int]map[int]bool)
	for i := 0; i < bytes; i++ {
		byte := puzzle.bytes[i]
		if m[byte[1]] == nil {
			m[byte[1]] = make(map[int]bool)
		}
		m[byte[1]][byte[0]] = true
	}
	return m
}

func (puzzle *Puzzle) printBytes(bytes int) string {
	m := puzzle.getMapAfterNBytes(bytes)
	result := ""
	for y := 0; y <= puzzle.height; y++ {
		for x := 0; x <= puzzle.width; x++ {
			if m[y][x] {
				result += "#"
			} else {
				result += "."
			}
		}
		result += "\n"
	}
	return result
}

func (puzzle *Puzzle) printPath(bytes int) string {
	m := puzzle.getMapAfterNBytes(bytes)

	p := make(map[int]map[int]bool)
	for _, point := range puzzle.findPath(bytes) {
		if p[point[1]] == nil {
			p[point[1]] = make(map[int]bool)
		}
		p[point[1]][point[0]] = true
	}

	result := ""
	for y := 0; y <= puzzle.height; y++ {
		for x := 0; x <= puzzle.width; x++ {
			if p[y][x] {
				result += "O"
			} else if m[y][x] {
				result += "#"
			} else {
				result += "."
			}
		}
		result += "\n"
	}
	return result
}

func (puzzle *Puzzle) findPath(bytes int) [][2]int {
	// map of corrupted points - [y][x]
	m := puzzle.getMapAfterNBytes(bytes)

	// map of points already checked - [y][x]
	w := make(map[int]map[int]bool)
	for y := 0; y <= puzzle.height; y++ {
		w[y] = make(map[int]bool)
		for x := 0; x <= puzzle.width; x++ {
			w[y][x] = false
		}
	}

	paths := [][][2]int{{{0, 0}}} // x,y
	w[0][0] = true

	directions := [][2]int{{1, 0}, {-1, 0}, {0, 1}, {0, -1}}
	for {
		var new_paths [][][2]int
		for _, path := range paths {
			last_point := path[len(path)-1]
			for _, direction := range directions {
				new_point := [2]int{last_point[0] + direction[0], last_point[1] + direction[1]}

				if new_point[0] < 0 || new_point[0] > puzzle.width {
					// out of bounds
					continue
				}
				if new_point[1] < 0 || new_point[1] > puzzle.height {
					// out of bounds
					continue
				}
				if m[new_point[1]][new_point[0]] {
					// corrupted byte
					continue
				}
				if w[new_point[1]][new_point[0]] {
					// already reached point
					continue
				}

				w[new_point[1]][new_point[0]] = true

				new_path := make([][2]int, len(path))
				copy(new_path, path)
				new_path = append(new_path, new_point)

				if new_point[0] == puzzle.width && new_point[1] == puzzle.height {
					return new_path
				}

				new_paths = append(new_paths, new_path)
			}
		}

		if len(new_paths) == 0 {
			return [][2]int{}
		}

		paths = new_paths
	}
}

func (puzzle *Puzzle) findBlockingByte() int {
	i := len(puzzle.bytes)
	for {
		i--
		if len(puzzle.findPath(i)) != 0 {
			return i
		}
	}
}

func main() {
	// puzzle := parseInput("input_demo")
	// puzzle.width = 6
	// puzzle.height = 6
	//
	// fmt.Println(puzzle.printBytes(12))
	// fmt.Println(puzzle.printPath(12))
	// fmt.Println("part1", len(puzzle.findPath(12))-1)
	// fmt.Println("part1", len(puzzle.findPath(12))-1)
	// fmt.Println("part2", puzzle.bytes[puzzle.findBlockingByte()])

	puzzle := parseInput("input")
	puzzle.width = 70
	puzzle.height = 70

	// fmt.Println(puzzle.printBytes(1024))
	// fmt.Println(puzzle.printPath(1024))
	fmt.Println("part1", len(puzzle.findPath(1024))-1)
	fmt.Println("part2", puzzle.bytes[puzzle.findBlockingByte()])
}
