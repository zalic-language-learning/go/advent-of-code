package main

import (
	"fmt"
	"os"
	"strings"
)

type Puzzle struct {
	start [2]int               // y, x
	end   [2]int               // y, x
	wall  map[int]map[int]bool // y, x
	// yes we lazy, yes we duplicate map as slice and map
	path    [][2]int            // y, x
	pathmap map[int]map[int]int // y, x = steps
}

var directions [][2]int = [][2]int{{1, 0}, {-1, 0}, {0, 1}, {0, -1}}

func parseInput(filename string) *Puzzle {
	input, err := os.ReadFile(filename)

	if err != nil {
		panic(err)
	}

	puzzle := Puzzle{
		wall:    make(map[int]map[int]bool),
		pathmap: make(map[int]map[int]int),
	}

	for y, line := range strings.Split(string(input), "\n") {
		for x, char := range line {
			if char == 'S' {
				puzzle.start = [2]int{y, x}
			} else if char == 'E' {
				puzzle.end = [2]int{y, x}
			} else if char == '#' {
				if puzzle.wall[y] == nil {
					puzzle.wall[y] = make(map[int]bool)
				}
				puzzle.wall[y][x] = true
			}
		}
	}

	puzzle.path = puzzle.findPath()
	for i, point := range puzzle.path {
		if puzzle.pathmap[point[0]] == nil {
			puzzle.pathmap[point[0]] = make(map[int]int)
		}
		puzzle.pathmap[point[0]][point[1]] = i
	}

	return &puzzle
}

func (puzzle *Puzzle) findPath() [][2]int {
	// map of points already checked - [y][x]
	w := make(map[int]map[int]bool)
	w[puzzle.start[0]] = make(map[int]bool)
	w[puzzle.start[0]][puzzle.start[1]] = true

	paths := [][][2]int{{{puzzle.start[0], puzzle.start[1]}}} // y, x

	for {
		var new_paths [][][2]int
		for _, path := range paths {
			last_point := path[len(path)-1]
			for _, direction := range directions {
				new_point := [2]int{last_point[0] + direction[0], last_point[1] + direction[1]}

				if puzzle.wall[new_point[0]][new_point[1]] {
					continue
				}

				if w[new_point[0]] == nil {
					w[new_point[0]] = make(map[int]bool)
				}
				if w[new_point[0]][new_point[1]] {
					continue
				}
				w[new_point[0]][new_point[1]] = true

				new_path := make([][2]int, len(path))
				copy(new_path, path)
				new_path = append(new_path, new_point)

				if puzzle.end[0] == new_point[0] && puzzle.end[1] == new_point[1] {
					return new_path
				}

				new_paths = append(new_paths, new_path)
			}
		}
		if len(new_paths) == 0 {
			return [][2]int{}
		}
		paths = new_paths
	}
}

type Shortcut struct {
	// point from where we start shortcut from
	from [2]int
	// wall that we go trough
	wall [2]int
	// destination point
	to [2]int
	// length of the race when we start shortcut
	start int
	// length of the race when we end shortcut
	end int
	// how many points we "cut" ( start - end - 2; ...because it takes to steps to execute the shortcut)
	save int
}

func (puzzle *Puzzle) findShortcuts() []Shortcut {
	var shortcuts []Shortcut
	for _, point := range puzzle.path {
		for _, direction := range directions {
			step_1 := [2]int{point[0] + direction[0], point[1] + direction[1]}
			step_2 := [2]int{point[0] + direction[0]*2, point[1] + direction[1]*2}

			if !puzzle.wall[step_1[0]][step_1[1]] {
				continue
			}

			end, ok := puzzle.pathmap[step_2[0]][step_2[1]]
			if !ok {
				continue
			}

			if start := puzzle.pathmap[point[0]][point[1]]; end > start {
				shortcut := Shortcut{
					from:  point,
					wall:  step_1,
					to:    step_2,
					start: start,
					end:   end,
					save:  end - start - 2,
				}
				shortcuts = append(shortcuts, shortcut)
			}
		}
	}
	return shortcuts
}

func (puzzle *Puzzle) String() string {
	result := ""
	height := len(puzzle.wall)
	width := len(puzzle.wall[0])
	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			if puzzle.start[0] == y && puzzle.start[1] == x {
				fmt.Print("S")
			} else if puzzle.end[0] == y && puzzle.end[1] == x {
				fmt.Print("E")
			} else if puzzle.wall[y][x] {
				fmt.Print("#")
			} else if point, ok := puzzle.pathmap[y][x]; ok {
				for point >= 10 {
					point -= 10
				}
				fmt.Print(point)
			} else {
				fmt.Print(" ")
			}
		}
		fmt.Print("\n")
	}
	return result
}

func main() {
	// puzzle := parseInput("input_demo")
	puzzle := parseInput("input")
	fmt.Println(puzzle)
	shortcuts := puzzle.findShortcuts()
	part1 := 0
	for _, shortcut := range shortcuts {
		if shortcut.save >= 100 {
			part1++
		}
		fmt.Println(shortcut.wall, shortcut.to, shortcut.save)
	}
	fmt.Println("part1", part1)
}
