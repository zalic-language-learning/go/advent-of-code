package main

import (
	"fmt"
	"math"
	"strconv"
)

type Point struct {
	x int
	y int
}

type Number struct {
	value              string
	points             []Point
	isAdjacentToSymbol bool
}

func isNumber(char string) bool {
	_, err := strconv.Atoi(char)
	return err == nil
}

func (c *PointsMap) isSymbol(x, y int) bool {
	char, err := c.get(x, y)
	if err != nil || char == "." || isNumber(char) {
		return false
	}
	return true
}

func (c *PointsMap) isAdjacentToASymbol(x, y int) bool {
	if c.isSymbol(x-1, y-1) || c.isSymbol(x-1, y) || c.isSymbol(x-1, y+1) || c.isSymbol(x, y-1) || c.isSymbol(x, y+1) || c.isSymbol(x+1, y-1) || c.isSymbol(x+1, y) || c.isSymbol(x+1, y+1) {
		return true
	}
	return false
}

func (c *PointsMap) part1() int {
	result := 0
	for y := 0; y < len(c.m); y++ {
		var number string
		isOK := false
		for x := 0; x < len(c.m[y]); x++ {
			char := c.m[y][x]
			if isNumber(char) {
				if c.isAdjacentToASymbol(x, y) {
					isOK = true
				}
				number += char
			} else {
				if len(number) > 0 {
					if isOK {
						nr, err := strconv.Atoi(number)
						if err != nil {
							panic(err)
						}
						result += nr
					}
					number = ""
					isOK = false
				}
			}
		}
		if len(number) > 0 {
			if isOK {
				nr, err := strconv.Atoi(number)
				if err != nil {
					panic(err)
				}
				result += nr
			}
		}
	}
	return result
}

func (c *PointsMap) getListOfNumbers() []Number {
	var numbers []Number
	for y := 0; y < len(c.m); y++ {
		var number Number
		for x := 0; x < len(c.m[y]); x++ {
			char := c.m[y][x]
			if isNumber(char) {
				number.points = append(number.points, Point{x: x, y: y})
				number.value += char
				if c.isAdjacentToASymbol(x, y) {
					number.isAdjacentToSymbol = true
				}
			} else {
				if len(number.points) > 0 {
					numbers = append(numbers, number)
					number = Number{}
				}
			}
		}
		if len(number.points) > 0 {
			numbers = append(numbers, number)
		}
	}
	return numbers
}

func part1(input []Number) int {
	result := 0
	for _, number := range input {
		if number.isAdjacentToSymbol {
			nr, err := strconv.Atoi(number.value)
			if err != nil {
				panic(err)
			}
			result += nr
		}
	}
	return result
}

func (c *PointsMap) part2(numbers []Number) int {
	result := 0
	for y := 0; y < len(c.m); y++ {
		for x := 0; x < len(c.m[y]); x++ {
			char := c.m[y][x]
			if char == "*" {
				var ids []int
				for index, nr := range numbers {
					isadj := false
					for _, point := range nr.points {
						if math.Abs(float64(point.x-x)) < 2 && math.Abs(float64(point.y-y)) < 2 {
							isadj = true
							break
						}
					}
					if isadj {
						ids = append(ids, index)
					}
				}
				if len(ids) == 2 {
					nr1, _ := strconv.Atoi(numbers[ids[0]].value)
					nr2, _ := strconv.Atoi(numbers[ids[1]].value)
					result += nr1 * nr2
				}
			}
		}
	}
	return result
}

func main() {
	demo := readFile("input_demo")
	demo_list := demo.getListOfNumbers()
	fmt.Println("demo part 1", part1(demo_list))
	fmt.Println("demo part 2", demo.part2(demo_list))

	input := readFile("input")
	input_list := input.getListOfNumbers()
	fmt.Println("part 1", part1(input_list))
	fmt.Println("part 2", input.part2(input_list))
}
