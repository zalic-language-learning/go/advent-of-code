package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
)

type PointsMap struct {
	m map[int]map[int]string
}

func (c *PointsMap) set(x, y int, value string) {
	if c.m == nil {
		c.m = map[int]map[int]string{}
	}
	if c.m[y] == nil {
		c.m[y] = map[int]string{}
	}
	c.m[y][x] = value
}

func (c *PointsMap) get(x, y int) (string, error) {
	if c.m == nil {
		var n string
		return n, errors.New("unset map point")
	}
	if _, ok := c.m[y]; ok {
		if value, ok := c.m[y][x]; ok {
			return value, nil
		}
	}
	var n string
	return n, errors.New("unset map point")
}

func (c *PointsMap) print() {
	for y := 0; y < len(c.m); y++ {
		line := ""
		for x := 0; x < len(c.m[y]); x++ {
			line += c.m[y][x]
		}
		fmt.Println(line)
	}
}

func readFile(filename string) PointsMap {
	dir, _ := os.Getwd()
	file := dir + "/" + filename

	f, err := os.Open(file)

	if err != nil {
		panic(err)
	}

	defer f.Close()

	fileScanner := bufio.NewScanner(f)
	fileScanner.Split(bufio.ScanBytes)

	const NEWLINE = string(byte('\n'))
	y := 0
	x := 0
	var result PointsMap

	for fileScanner.Scan() {
		byte := fileScanner.Text()
		if byte == NEWLINE {
			x = 0
			y++
			continue
		}
		result.set(x, y, string(byte))
		x++
	}

	return result
}
