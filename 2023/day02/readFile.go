package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Game struct {
	gameId int
	red    int
	green  int
	blue   int
}

type Pull struct {
	red   int
	green int
	blue  int
}

// parses a single pull in a game
// e.g "2 blue, 1 red, 2 green"
func parsePull(input string) Pull {
	result := Pull{}
	for _, x := range strings.Split(input, ", ") {
		pair := strings.Split(x, " ")

		amount, err := strconv.Atoi(pair[0])
		if err != nil {
			panic(fmt.Sprintf("failed to parse pairi %s", input))
		}

		switch pair[1] {
		case "red":
			result.red = amount
		case "blue":
			result.blue = amount
		case "green":
			result.green = amount
		}
	}

	return result
}

func parseLine(line string) Game {
	var game = Game{}

	cp := strings.Index(line, ":")

	gameId, err := strconv.Atoi(line[5:cp])
	if err != nil {
		panic(err)
	}
	game.gameId = gameId

	// parse each pull per game
	for _, x := range strings.Split(line[cp+2:], "; ") {
		// store the larges pull per color on game object
		pull := parsePull(x)
		if pull.red > game.red {
			game.red = pull.red
		}
		if pull.green > game.green {
			game.green = pull.green
		}
		if pull.blue > game.blue {
			game.blue = pull.blue
		}
	}

	return game
}

func readFile(filename string) []Game {
	dir, _ := os.Getwd()
	file := dir + "/" + filename

	f, err := os.Open(file)

	if err != nil {
		panic(err)
	}

	defer f.Close()

	fileScanner := bufio.NewScanner(f)
	fileScanner.Split(bufio.ScanLines)

	var result []Game

	// Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
	for fileScanner.Scan() {
		line := fileScanner.Text()
		result = append(result, parseLine(line))
	}

	return result
}
