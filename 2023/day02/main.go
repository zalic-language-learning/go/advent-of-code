package main

import "fmt"

func getPossibleGameIdsSum(games []Game) int {
	sum := 0
	for _, game := range games {
		if game.red <= 12 && game.green <= 13 && game.blue <= 14 {
			sum += game.gameId
		}
	}
	return sum
}
func getGamesPower(games []Game) int {
	power := 0
	for _, game := range games {
		power += game.red * game.green * game.blue
	}
	return power
}

func main() {
	input_demo := readFile("input_demo")
	fmt.Println("demo part 1", getPossibleGameIdsSum(input_demo))
	fmt.Println("demo part 2", getGamesPower(input_demo))

	input := readFile("input")
	fmt.Println("part 1", getPossibleGameIdsSum(input))
	fmt.Println("part 2", getGamesPower(input))
}
