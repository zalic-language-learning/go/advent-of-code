package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParsePull(t *testing.T) {
	cases := map[string]Pull{
		"2 blue, 1 red, 2 green": Pull{red: 1, green: 2, blue: 2},
	}
	for input, want := range cases {
		result := parsePull(input)
		assert.Equal(t, want, result, "Failed to parse pull.")
	}
}

func TestParseLine(t *testing.T) {
	cases := map[string]Game{
		"Game 55: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green": Game{gameId: 55, red: 6, green: 3, blue: 2},
	}
	for input, want := range cases {
		result := parseLine(input)
		assert.Equal(t, want, result, "Failed to parse line.")
	}
}
