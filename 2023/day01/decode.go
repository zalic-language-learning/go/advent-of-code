package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func getNumbers(input string) []int {
	var result []int
	for _, c := range input {
		if n, err := strconv.Atoi(string(c)); err == nil {
			result = append(result, n)
		}
	}
	return result
}

// take the first and the last number and concat them
// []int{1,2,3} -> 13
// []int{1} -> 11
func getCombinationNumber(input []int) int {
	firstNumber := input[0]
	lastNumber := input[len(input)-1]

	result, err := strconv.Atoi(fmt.Sprintf("%d%d", firstNumber, lastNumber))
	if err != nil {
		panic(err)
	}

	return result
}

func decodeFromFile(filename string) int {
	dir, _ := os.Getwd()
	file := dir + "/" + filename

	f, err := os.Open(file)

	if err != nil {
		panic(err)
	}

	defer f.Close()

	fileScanner := bufio.NewScanner(f)
	fileScanner.Split(bufio.ScanLines)

	var result int

	for fileScanner.Scan() {
		line := fileScanner.Text()

		numbers := getNumbers(line)

		if len(numbers) < 1 {
			log.Printf("line \"%s\" doesnt contain any numbers", line)
			continue
		}

		result += getCombinationNumber(numbers)
	}

	return result
}
