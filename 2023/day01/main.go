package main

import "fmt"

func main() {
	fmt.Println("demo", decodeFromFile("input_demo"))
	fmt.Println("part 1", decodeFromFile("input"))
}
