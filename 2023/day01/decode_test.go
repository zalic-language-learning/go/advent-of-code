package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGetNumbers(t *testing.T) {
	cases := map[string][]int{
		"t1es2t": {1, 2},
		"123456": {1, 2, 3, 4, 5, 6},
	}

	for input, want := range cases {
		result := getNumbers(input)
		assert.Equal(t, want, result, "The two words should be the same.")
	}
}

func TestGetCombinationNumber(t *testing.T) {
	cases := map[int][]int{
		11: {1},
		12: {1, 2},
		16: {1, 2, 3, 4, 5, 6},
	}

	for want, input := range cases {
		result := getCombinationNumber(input)
		assert.Equal(t, want, result, "Failed to combine numbers")
	}
}
