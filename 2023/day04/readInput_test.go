package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseLine(t *testing.T) {
	cases := map[string]Card{
		"Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1":  {id: 3, numbers: [][]int{{1, 21, 53, 59, 44}, {69, 82, 63, 72, 16, 21, 14, 1}}},
		"Card 99:  1 21 53 59 44 | 69 82 63 72 16 21 14  1": {id: 99, numbers: [][]int{{1, 21, 53, 59, 44}, {69, 82, 63, 72, 16, 21, 14, 1}}},
	}
	for input, want := range cases {
		result := parseLine(input)
		assert.Equal(t, want, result, "Failed to parse line.")
	}
}
