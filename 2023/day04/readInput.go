package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Card struct {
	id      int
	numbers [][]int
}

func parseNumbers(input string) []int {
	var result []int
	list := strings.Split(input, " ")
	for _, n := range list {
		nr, err := strconv.Atoi(n)
		if err != nil {
			continue
		}
		result = append(result, nr)
	}
	return result
}

func parseLine(line string) Card {
	line = strings.ReplaceAll(line, "  ", " ")

	var result Card

	cp := strings.Index(line, ":")

	cardId := line[5:cp]
	cardId = strings.ReplaceAll(cardId, " ", "")
	id, err := strconv.Atoi(cardId)
	if err != nil {
		panic(fmt.Sprintf("Failed to parse card id from '%s', err: %s", cardId, err))
	}
	result.id = id

	parts := strings.Split(line[cp+1:], " | ")

	if len(parts) != 2 {
		panic(fmt.Sprintf("expecting two sets of numbers, found %d", len(parts)))
	}

	result.numbers = append(result.numbers, parseNumbers(parts[0]))
	result.numbers = append(result.numbers, parseNumbers(parts[1]))

	return result
}

func readInput(filename string) []Card {
	dir, _ := os.Getwd()
	file := dir + "/" + filename

	f, err := os.Open(file)

	if err != nil {
		panic(err)
	}

	defer f.Close()

	fileScanner := bufio.NewScanner(f)
	fileScanner.Split(bufio.ScanLines)

	var result []Card

	for fileScanner.Scan() {
		line := fileScanner.Text()
		result = append(result, parseLine(line))
	}

	return result
}
