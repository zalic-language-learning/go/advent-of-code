package main

import "fmt"

func part1GetCardPoints(card Card) int {
	result := 0
	for i := 0; i < len(card.numbers[0]); i++ {
		n1 := card.numbers[0][i]
		win := false
		for j := 0; j < len(card.numbers[1]); j++ {
			n2 := card.numbers[1][j]
			if n1 == n2 {
				win = true
			}
		}
		if win {
			if result == 0 {
				result = 1
			} else {
				result = result * 2
			}
		}
	}
	return result
}

func part1(cards []Card) int {
	result := 0
	for _, card := range cards {
		result += part1GetCardPoints(card)
	}
	return result
}

func main() {
	cards_demo := readInput("input_demo")
	fmt.Println("demo part1", part1(cards_demo))
	fmt.Println("---")
	cards := readInput("input")
	fmt.Println("part1", part1(cards))
}
