package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Range struct {
	dest   int
	source int
	length int
}
type RangeList []Range

type Map map[string]map[string]RangeList

type Result struct {
	seeds   []int
	mapping Map
}

func (rangeList *RangeList) getVal(in int) int {
	for _, r := range *rangeList {
		if in >= r.source && in < r.source+r.length {
			return in + (r.dest - r.source)
		}
	}
	return in
}

func isNumber(char string) bool {
	_, err := strconv.Atoi(char)
	return err == nil
}

func parseNumberList(input string) []int {
	result := []int{}
	for _, n := range strings.Split(input, " ") {
		nr, err := strconv.Atoi(n)
		if err != nil {
			panic(fmt.Sprintf("Non numeric item in list '%s' err: %s", input, err))
		}
		result = append(result, nr)
	}
	return result
}

func readInput(filename string) Result {
	dir, _ := os.Getwd()
	file := dir + "/" + filename

	f, err := os.Open(file)

	if err != nil {
		panic(err)
	}

	defer f.Close()

	fileScanner := bufio.NewScanner(f)
	fileScanner.Split(bufio.ScanLines)

	var result Result
	result.mapping = make(Map)

	var activeListSource string
	var activeListDest string

	for fileScanner.Scan() {
		line := fileScanner.Text()

		switch {
		case line == "":
		case line[0:6] == "seeds:":
			result.seeds = parseNumberList(line[7:])

		// if the first char of the line is a number, its a map
		case isNumber(line[0:1]):
			n := parseNumberList(line)
			if len(n) != 3 {
				panic(fmt.Sprintf("map '%s' should have exatly 3 numbers, err: %s", line, err))
			}

			if result.mapping[activeListSource] == nil {
				result.mapping[activeListSource] = map[string]RangeList{}
			}

			if result.mapping[activeListSource][activeListDest] == nil {
				result.mapping[activeListSource][activeListDest] = []Range{}
			}

			result.mapping[activeListSource][activeListDest] = append(result.mapping[activeListSource][activeListDest], Range{dest: n[0], source: n[1], length: n[2]})

		// if a string, its the title of a new list
		default:
			// remove " map:" suffix
			// and split from "-to-"
			p := strings.Split(line[0:len(line)-5], "-to-")
			if len(p) != 2 {
				panic(fmt.Sprintf("failed to parse map title '%s'", line))
			}
			activeListSource = p[0]
			activeListDest = p[1]
		}
	}

	return result
}
