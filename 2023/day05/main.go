package main

import (
	"fmt"
	"math"
)

type Item map[string]int

func parsePart1(input Result) []Item {
	var ret []Item = []Item{}
	for _, seed := range input.seeds {
		var result Item = Item{}
		key := "seed"
		result["seed"] = seed
		for input.mapping[key] != nil {
			// this only works because every propery has only one child
			for childKey := range input.mapping[key] {
				m := input.mapping[key][childKey]
				result[childKey] = m.getVal(result[key])
				key = childKey
			}
		}
		ret = append(ret, result)
	}
	return ret
}

func findSmallestLoc(input []Item) int {
	min := math.MaxInt
	for _, item := range input {
		if item["location"] < min {
			min = item["location"]
		}
	}
	return min
}

func main() {
	demo := readInput("input_demo")
	fmt.Println("demo part 1", findSmallestLoc(parsePart1(demo)))
	fmt.Println("---")
	input := readInput("input")
	fmt.Println("part 1", findSmallestLoc(parsePart1(input)))
}
