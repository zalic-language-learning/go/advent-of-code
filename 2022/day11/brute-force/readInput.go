package main

import (
	"bufio"
	"fmt"
	"math/big"
	"os"
	"strconv"
	"strings"
)

type Monkey struct {
	// list of items monkey holds, identified by worry level
	items           []*big.Int
	operation       string
	operationArgA   *big.Int
	operationArgB   *big.Int
	testDivisibleBy *big.Int
	testTrue        int
	testFalse       int
	inspectCount    uint64
}

func (m *Monkey) getMonkeyToThrowTo(itemWorryLevel *big.Int) int {
	z := big.NewInt(0)
	z.Mod(itemWorryLevel, m.testDivisibleBy)
	if z.Cmp(big.NewInt(0)) == 0 {
		return m.testTrue
	} else {
		return m.testFalse
	}
}

func (m *Monkey) addItem(itemWorryLevel *big.Int) {
	m.items = append(m.items, itemWorryLevel)
}

func (m *Monkey) getNewItemWorryLevel(itemWorryLevel *big.Int) *big.Int {
	m.inspectCount++

	a := itemWorryLevel
	if m.operationArgA != nil {
		a = m.operationArgA
	}
	b := itemWorryLevel
	if m.operationArgB != nil {
		b = m.operationArgB
	}

	switch m.operation {
	case "+":
		z := big.NewInt(0)
		z.Add(a, b)
		return z
	case "-":
		z := big.NewInt(0)
		z.Sub(a, b)
		return z
	case "*":
		z := big.NewInt(0)
		z.Mul(a, b)
		return z
	case "/":
		z := big.NewInt(0)
		z.Div(a, b)
		return z
	default:
		panic(fmt.Sprint("invalid operation", m.operation))
	}
}

func readInput(filename string) map[int]*Monkey {
	result := map[int]*Monkey{}

	dir, _ := os.Getwd()
	file := dir + "/" + filename

	f, err := os.Open(file)

	if err != nil {
		panic(err)
	}

	defer f.Close()

	fileScanner := bufio.NewScanner(f)
	fileScanner.Split(bufio.ScanLines)

	currentMonkeyId := 0
	var currentMonkey *Monkey
	for fileScanner.Scan() {
		line := fileScanner.Text()
		switch {
		case line == "":

		case len(line) > 7 && line[0:6] == "Monkey":
			// there are no doubble digit monkey ids in this test
			// we COULD check not to write first empty monkey to 0 index
			result[currentMonkeyId] = currentMonkey
			currentMonkey = new(Monkey)
			n, _ := strconv.Atoi(line[7:8])
			currentMonkeyId = n
			//
		case len(line) > 17 && line[0:17] == "  Starting items:":
			if currentMonkey.items == nil {
				currentMonkey.items = []*big.Int{}
			}
			for _, n := range strings.Split(line[18:], ", ") {
				i, _ := strconv.Atoi(n)
				currentMonkey.items = append(currentMonkey.items, big.NewInt(int64(i)))
			}
		case len(line) > 12 && line[0:19] == "  Operation: new = ":
			parts := strings.Split(line[19:], " ")
			currentMonkey.operation = parts[1]
			if parts[0] != "old" {
				n, _ := strconv.Atoi(parts[0])
				z := big.NewInt(int64(n))
				currentMonkey.operationArgA = z
			}
			if parts[2] != "old" {
				n, _ := strconv.Atoi(parts[2])
				z := big.NewInt(int64(n))
				currentMonkey.operationArgB = z
			}
		case len(line) > 21 && line[0:21] == "  Test: divisible by ":
			n, _ := strconv.Atoi(line[21:])
			z := big.NewInt(int64(n))
			currentMonkey.testDivisibleBy = z
		case len(line) > 29 && line[0:29] == "    If true: throw to monkey ":
			n, _ := strconv.Atoi(line[29:])
			currentMonkey.testTrue = n
		case len(line) > 30 && line[0:30] == "    If false: throw to monkey ":
			n, _ := strconv.Atoi(line[30:])
			currentMonkey.testFalse = n
		default:
			fmt.Println("unparsed line", line)
		}
	}
	result[currentMonkeyId] = currentMonkey
	return result
}
