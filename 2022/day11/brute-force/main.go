package main

import (
	"fmt"
	"log"
	"math/big"
)

type sortedSlice [2]uint64

func (list *sortedSlice) add(n uint64) {
	var addValue func(n uint64, index int)

	addValue = func(n uint64, i int) {
		if i >= cap(list) {
			return
		}

		currentValue := list[i]

		if currentValue < n {
			list[i] = n
			n = currentValue
		}

		addValue(n, i+1)
	}

	addValue(n, 0)
}

func printInventory(monkeys map[int]*Monkey) {
	for i := 0; i < len(monkeys); i++ {
		fmt.Println("Monkey ", i, " inventory", monkeys[i].items)
	}
}
func printInspectCount(monkeys map[int]*Monkey) {
	var inspections sortedSlice
	for i := 0; i < len(monkeys); i++ {
		inspections.add(monkeys[i].inspectCount)
		fmt.Printf("Monkey %d inspected items %d times.\n", i, monkeys[i].inspectCount)
	}
	log.Println("result for part 1:", inspections[0]*inspections[1])
}

func playRound(monkeys map[int]*Monkey, divideByThree, print bool) {
	for i := 0; i < len(monkeys); i++ {
		for _, worry := range monkeys[i].items {

			newItemWorryLevel := monkeys[i].getNewItemWorryLevel(worry)

			if print {
				fmt.Println("\tinput item worry:", worry)
				fmt.Println("\t\tnew item worry:", newItemWorryLevel)
			}

			if divideByThree {
				z := big.NewInt(0)
				z = z.Div(newItemWorryLevel, big.NewInt(3))
				newItemWorryLevel = z
				if print {
					fmt.Println("\t\tdivideByThree:", newItemWorryLevel)
				}
			}

			newOwnerId := monkeys[i].getMonkeyToThrowTo(newItemWorryLevel)

			if print {
				fmt.Println("\t\tthrow to monkey:", newOwnerId)
			}

			monkeys[newOwnerId].addItem(newItemWorryLevel)
			monkeys[i].items = monkeys[i].items[1:]
		}
	}
}

func main() {
	monkeys := readInput("input")

	for i := 0; i < 20; i++ {
		playRound(monkeys, true, false)
	}
	printInspectCount(monkeys)

	part2 := readInput("input")
	for i := 0; i <= 10000; i++ {
		if i == 1 || i == 20 || i == 50 || i == 100 || i == 1000 || i == 10000 {
			fmt.Println("ROUND", i)
			printInspectCount(part2)
		}
		playRound(part2, false, false)
	}
}
