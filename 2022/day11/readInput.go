package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var bigMod int64 = 1

type MonkeyId uint8

type Monkey struct {
	// list of items monkey holds, identified by worry level
	items           []int64
	operation       string
	operationArgA   *int64
	operationArgB   *int64
	testDivisibleBy int64
	testTrue        MonkeyId
	testFalse       MonkeyId
	inspectCount    int64
}

func (m *Monkey) getMonkeyToThrowTo(itemWorryLevel int64) MonkeyId {
	if itemWorryLevel%m.testDivisibleBy == 0 {
		return m.testTrue
	} else {
		return m.testFalse
	}
}

func (m *Monkey) addItem(itemWorryLevel int64) {
	m.items = append(m.items, itemWorryLevel)
}

func (m *Monkey) getNewItemWorryLevel(itemWorryLevel int64) int64 {
	m.inspectCount++

	a := itemWorryLevel
	if m.operationArgA != nil {
		a = *m.operationArgA
	}
	b := itemWorryLevel
	if m.operationArgB != nil {
		b = *m.operationArgB
	}

	var val int64
	switch m.operation {
	case "+":
		val = a + b
	case "-":
		val = a - b
	case "*":
		val = a * b
	case "/":
		val = a / b
	default:
		panic(fmt.Sprint("invalid operation", m.operation))
	}
	return val % bigMod
}

func readInput(filename string) map[MonkeyId]*Monkey {
	result := map[MonkeyId]*Monkey{}

	dir, _ := os.Getwd()
	file := dir + "/" + filename

	f, err := os.Open(file)

	if err != nil {
		panic(err)
	}

	defer f.Close()

	fileScanner := bufio.NewScanner(f)
	fileScanner.Split(bufio.ScanLines)

	currentMonkeyId := MonkeyId(0)
	var currentMonkey *Monkey
	for fileScanner.Scan() {
		line := fileScanner.Text()
		switch {
		case line == "":

		case len(line) > 7 && line[0:6] == "Monkey":
			// there are no doubble digit monkey ids in this test
			// we COULD check not to write first empty monkey to 0 index
			result[currentMonkeyId] = currentMonkey
			currentMonkey = new(Monkey)
			n, _ := strconv.Atoi(line[7:8])
			currentMonkeyId = MonkeyId(n)
			//
		case len(line) > 17 && line[0:17] == "  Starting items:":
			if currentMonkey.items == nil {
				currentMonkey.items = []int64{}
			}
			for _, n := range strings.Split(line[18:], ", ") {
				i, _ := strconv.Atoi(n)
				currentMonkey.items = append(currentMonkey.items, int64(i))
			}
		case len(line) > 12 && line[0:19] == "  Operation: new = ":
			parts := strings.Split(line[19:], " ")
			currentMonkey.operation = parts[1]
			if parts[0] != "old" {
				n, _ := strconv.Atoi(parts[0])
				z := int64(n)
				currentMonkey.operationArgA = &z
			}
			if parts[2] != "old" {
				n, _ := strconv.Atoi(parts[2])
				z := int64(n)
				currentMonkey.operationArgB = &z
			}
		case len(line) > 21 && line[0:21] == "  Test: divisible by ":
			n, _ := strconv.Atoi(line[21:])
			z := int64(n)
			currentMonkey.testDivisibleBy = z
		case len(line) > 29 && line[0:29] == "    If true: throw to monkey ":
			n, _ := strconv.Atoi(line[29:])
			currentMonkey.testTrue = MonkeyId(n)
		case len(line) > 30 && line[0:30] == "    If false: throw to monkey ":
			n, _ := strconv.Atoi(line[30:])
			currentMonkey.testFalse = MonkeyId(n)
		default:
			fmt.Println("unparsed line", line)
		}
	}
	result[currentMonkeyId] = currentMonkey

	bigMod = 1
	for _, monkey := range result {
		bigMod *= monkey.testDivisibleBy
	}

	return result
}
