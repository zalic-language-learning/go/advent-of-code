package main

import (
	"fmt"
)

type sortedSlice [2]int64

func (list *sortedSlice) add(n int64) {
	var addValue func(n int64, index int)

	addValue = func(n int64, i int) {
		if i >= cap(list) {
			return
		}

		currentValue := list[i]

		if currentValue < n {
			list[i] = n
			n = currentValue
		}

		addValue(n, i+1)
	}

	addValue(n, 0)
}

func printInventory(monkeys map[MonkeyId]*Monkey) {
	for i := 0; i < len(monkeys); i++ {
		monkeyId := MonkeyId(i)
		fmt.Println("Monkey ", i, " inventory", monkeys[monkeyId].items)
	}
}
func printInspectCount(monkeys map[MonkeyId]*Monkey) {
	var inspections sortedSlice
	for i := 0; i < len(monkeys); i++ {
		monkeyId := MonkeyId(i)
		inspections.add(monkeys[monkeyId].inspectCount)
		fmt.Printf("Monkey %d inspected items %d times.\n", i, monkeys[monkeyId].inspectCount)
	}
	fmt.Println("monkey business:", inspections[0]*inspections[1])
}

func playRound(monkeys map[MonkeyId]*Monkey, divideByThree, print bool) {
	for i := 0; i < len(monkeys); i++ {
		monkeyId := MonkeyId(i)

		if print {
			fmt.Println("monkey:", monkeyId)
		}

		for _, worry := range monkeys[monkeyId].items {

			newItemWorryLevel := monkeys[monkeyId].getNewItemWorryLevel(worry)

			if print {
				fmt.Println("\tinput item worry:", worry)
				fmt.Println("\t\tnew item worry:", newItemWorryLevel)
			}

			if divideByThree {
				newItemWorryLevel = newItemWorryLevel / 3
				if print {
					fmt.Println("\t\tdivideByThree:", newItemWorryLevel)
				}
			}

			newOwnerId := monkeys[monkeyId].getMonkeyToThrowTo(newItemWorryLevel)

			if print {
				fmt.Println("\t\tthrow to monkey:", newOwnerId)
			}

			monkeys[newOwnerId].addItem(newItemWorryLevel)
			// monkeys[monkeyId].items = monkeys[monkeyId].items[1:]
			monkeys[monkeyId].items = []int64{}
		}
	}
}

func part1(file string) {
	monkeys := readInput(file)

	for i := 0; i < 20; i++ {
		playRound(monkeys, true, false)
	}
	printInspectCount(monkeys)
}
func part2(file string) {
	part2 := readInput(file)
	for i := 0; i < 10000; i++ {
		/*
			if i == 1 || i == 20 || i == 50 || i == 100 || i == 1000 || i == 2000 || i == 3000 || i == 4000 || i == 5000 || i == 6000 || i == 7000 || i == 8000 || i == 9000 {
				fmt.Println("ROUND", i)
				printInspectCount(part2)
			}
		*/
		playRound(part2, false, false)
	}
	fmt.Println("ROUND 10 000")
	printInspectCount(part2)
}

func main() {
	input := "input"
	fmt.Println("PART 1")
	part1(input)
	fmt.Println("PART 2")
	part2(input)
}
