package main

import (
	"bufio"
	"errors"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type Group struct {
	start int
	end   int
}
type Pair struct {
	e1 Group
	e2 Group
}

func parseGroup(input string) (Group, error) {
	group := strings.Split(input, "-")
	if len(group) != 2 {
		return Group{}, errors.New("")
	}
	start, err1 := strconv.Atoi(group[0])
	end, err2 := strconv.Atoi(group[1])
	if err1 != nil || err2 != nil {
		return Group{}, errors.New("")
	}
	return Group{start: start, end: end}, nil
}
func parseLine(line string) (Pair, error) {
	pairs := strings.Split(line, ",")
	if len(pairs) != 2 {
		return Pair{}, errors.New(fmt.Sprintf("Invalid input line: %s", line))
	}
	e1, err1 := parseGroup(pairs[0])
	e2, err2 := parseGroup(pairs[1])
	if err1 != nil || err2 != nil {
		return Pair{}, errors.New(fmt.Sprintf("Invalid input line: %s", line))
	}
	return Pair{
		e1: e1,
		e2: e2,
	}, nil
}

func main() {
	cwd, _ := os.Getwd()
	filename := cwd + "/input"

	f, err := os.Open(filename)

	if err != nil {
		panic(err)
	}

	defer f.Close()

	fileScanner := bufio.NewScanner(f)
	fileScanner.Split(bufio.ScanLines)

	total := 0
	for fileScanner.Scan() {
		line := fileScanner.Text()
		pair, err := parseLine(line)
		if err != nil {
			log.Println("invalid line: ", line)
			continue
		}
		if (pair.e1.start <= pair.e2.start && pair.e1.end >= pair.e2.end) || (pair.e1.start >= pair.e2.start && pair.e1.end <= pair.e2.end) {
			// fmt.Println("yup", line, pair)
			total += 1
		} else {
			// fmt.Println("nop", line, pair)
		}
	}
	fmt.Println("total", total)
}
