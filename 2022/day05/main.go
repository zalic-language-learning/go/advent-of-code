package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	cwd, _ := os.Getwd()
	filename := cwd + "/input"

	f, err := os.Open(filename)

	if err != nil {
		panic(err)
	}

	defer f.Close()

	fileScanner := bufio.NewScanner(f)
	fileScanner.Split(bufio.ScanLines)

	// map[COLUMN_NR (starting from 1)] = slice of runes,
	// smallest index is the topmost package
	var crates = map[int][]rune{}
	findCrates := func(line string) {
		for n, c := range line {
			if c == '[' {
				// column, starting from 1
				column := ((n + 1) / 4) + 1
				// fmt.Println(fmt.Sprintf("%d. %c", column, line[n+1]), line[n+1])
				col, ok := crates[column]
				if !ok {
					col = []rune{}
				}
				col = append(col, rune(line[n+1]))
				crates[column] = col
			}
		}
	}

	for fileScanner.Scan() {
		line := fileScanner.Text()
		args := strings.Split(line, " ")
		if args[0] == "move" {
			amount, _ := strconv.Atoi(args[1])
			from, _ := strconv.Atoi(args[3])
			to, _ := strconv.Atoi(args[5])

			// fmt.Println(crates)

			// move := crates[from][0:amount]
			// "clone" the slice not reference
			move := append([]rune{}, crates[from][0:amount]...)

			crates[from] = crates[from][len(move):]

			// Can use slices.Reverse in Go 1.21 or later
			// crates[to] = append(slices.Reverse(move), crates[to]...)

			/*
				for _, crate := range move {
					crates[to] = append([]rune{crate}, crates[to]...)
				}
			*/

			// part2:
			crates[to] = append(move, crates[to]...)

			// fmt.Println(crates, move)
		} else {
			findCrates(line)
		}
	}
	result := ""
	for i := 1; i <= len(crates); i++ {
		result += string(crates[i][0])
	}
	fmt.Println("result", result)
}
