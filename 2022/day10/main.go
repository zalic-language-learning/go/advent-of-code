package main

import (
	"fmt"
	"sync"
)

// X, which starts with the value 1.
// addx V takes two cycles to complete.
// After two cycles, the X register is increased by the value V. (V can be negative.)
// noop takes one cycle to complete. It has no other effect.

// Maybe you can learn something by looking at the value of the X register throughout execution.
// For now, consider the signal strength (the cycle number multiplied by the value of the X register)
// during the 20th cycle and every 40 cycles after that (that is, during the 20th, 60th, 100th, 140th, 180th, and 220th cycles).

type Cycle struct {
	CycleNr int
	X       int
}

// processes commands from commands channel
// sends registry value to cycles channel for every cycle
func runCommands(commands <-chan Command, cycles chan<- int) {
	X := 1
	cycles <- X
	for {
		command, open := <-commands
		if !open {
			close(cycles)
			break
		}
		if command.t == noop {
			cycles <- X
		}
		if command.t == addx {
			cycles <- X
			X = X + *command.v
			cycles <- X
		}
	}
}

func runCycles(wg *sync.WaitGroup, cycles <-chan int, out chan<- string) {
	cycleNr := 0
	counterForPart1 := 20
	totalForPart1 := 0
	output := ""
	linePointer := 0
	lines := 0

	for {
		value, open := <-cycles
		if !open {
			break
		}

		if counterForPart1 == 0 {
			signalStrength := cycleNr * value
			totalForPart1 += signalStrength
			fmt.Println(fmt.Sprintf("cycleNr: %d registryValue: %d signalStrength: %d Total ( answer to part 1 ) %d", cycleNr, value, signalStrength, totalForPart1))
			counterForPart1 = 40
		}
		counterForPart1--
		cycleNr++

		if value == linePointer || value+1 == linePointer || value-1 == linePointer {
			output += "#"
		} else {
			output += "."
		}

		linePointer++
		if linePointer == 40 {
			output += "\n"
			linePointer = 0
			lines++
			if lines == 6 {
				out <- output
				lines = 0
			}
		}
	}

	wg.Done()
}

func main() {
	wg := sync.WaitGroup{}
	wg.Add(1)
	commands := make(chan Command)
	cycles := make(chan int)
	output := make(chan string, 1)
	go runCycles(&wg, cycles, output)
	go runCommands(commands, cycles)
	getCommands("input", commands)
	wg.Wait()
	fmt.Println()
	fmt.Println(<-output)
}
