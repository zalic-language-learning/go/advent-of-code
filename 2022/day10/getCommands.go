package main

import (
	"bufio"
	"os"
	"strconv"
	"strings"
)

type CommandType = int8

const (
	noop CommandType = iota
	addx             = iota
)

type Command struct {
	t CommandType
	v *int
}

func getCommands(filename string, commands chan<- Command) {
	dir, _ := os.Getwd()
	file := dir + "/" + filename

	f, err := os.Open(file)

	if err != nil {
		panic(err)
	}

	defer f.Close()

	fileScanner := bufio.NewScanner(f)
	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		line := fileScanner.Text()
		if line == "noop" {
			commands <- Command{t: noop}
			continue
		}
		parts := strings.Split(line, " ")
		v, _ := strconv.Atoi(parts[1])
		commands <- Command{t: addx, v: &v}
	}
	close(commands)
}
