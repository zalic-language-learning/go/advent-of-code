package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"unicode"
)

func findMatch(s string) rune {
	l := len(s)
	pl := l / 2
	for i := 0; i < pl; i++ {
		for j := pl; j < l; j++ {
			if s[i] == s[j] {
				return rune(s[i])
			}
		}
	}
	return -1
}

func main() {
	letters := map[rune]int{}
	for c, i := 'a', 1; c <= 'z'; c, i = c+1, i+1 {
		letters[c] = i
		letters[unicode.ToUpper(c)] = i + 26
	}

	// for c, i := range letters {
	// fmt.Println(fmt.Sprintf("%d. %s", i, string(c)))
	// }

	cwd, _ := os.Getwd()
	filename := cwd + "/input"

	f, err := os.Open(filename)

	if err != nil {
		panic(err)
	}

	defer f.Close()

	fileScanner := bufio.NewScanner(f)
	fileScanner.Split(bufio.ScanLines)

	total := 0
	for fileScanner.Scan() {
		line := fileScanner.Text()
		match := findMatch(line)
		if match == -1 {
			log.Fatal("Could not find match in ", line)
		}

		value, ok := letters[match]
		if !ok {
			log.Fatal(fmt.Sprintf("The letter '%c' has not been mapped", match))
		}

		// fmt.Println(line, string(match), letters[match])
		total += value
	}
	fmt.Println("total", total)
}
