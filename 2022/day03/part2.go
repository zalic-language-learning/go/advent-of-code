package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"unicode"
)

func contains(s string, e rune) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func findMatchingChar(s1, s2, s3 string) rune {
	for _, c := range s1 {
		if contains(s2, c) && contains(s3, c) {
			return c
		}
	}
	return -1
}

func main() {
	letters := map[rune]int{}
	for c, i := 'a', 1; c <= 'z'; c, i = c+1, i+1 {
		letters[c] = i
		letters[unicode.ToUpper(c)] = i + 26
	}

	// for c, i := range letters {
	// fmt.Println(fmt.Sprintf("%d. %s", i, string(c)))
	// }

	cwd, _ := os.Getwd()
	filename := cwd + "/input"

	f, err := os.Open(filename)

	if err != nil {
		panic(err)
	}

	defer f.Close()

	fileScanner := bufio.NewScanner(f)
	fileScanner.Split(bufio.ScanLines)

	total := 0
	group := []string{}
	for fileScanner.Scan() {
		line := fileScanner.Text()
		group = append(group, line)

		if len(group) == 3 {
			c := findMatchingChar(group[0], group[1], group[2])

			value, ok := letters[c]
			if !ok {
				log.Fatal(fmt.Sprintf("The letter '%c' has not been mapped", c))
			}

			total += value
			// fmt.Println(group, string(c))
			group = nil
		}
	}
	fmt.Println("total", total)
}
