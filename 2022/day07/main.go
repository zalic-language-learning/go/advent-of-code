package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Directory struct {
	name   string
	files  map[string]*File
	dirs   map[string]*Directory
	parent *Directory
}

type File struct {
	name string
	size int
}

func (d *Directory) size() int {
	size := 0
	for _, file := range d.files {
		size += file.size
	}
	for _, dir := range d.dirs {
		size += dir.size()
	}
	return size
}

func (d *Directory) cd(dir string) *Directory {
	if d, ok := d.dirs[dir]; ok {
		return d
	}
	newDirectory := Directory{
		name:   dir,
		parent: d,
	}
	if d.dirs == nil {
		d.dirs = map[string]*Directory{}
	}
	d.dirs[dir] = &newDirectory
	return &newDirectory
}

func (d *Directory) addFile(fileName string, fileSize int) {
	if _, ok := d.files[fileName]; ok {
		return
	}

	file := File{
		name: fileName,
		size: fileSize,
	}

	if d.files == nil {
		d.files = map[string]*File{}
	}

	d.files[fileName] = &file
}

func (d *Directory) printTree() string {
	var printTree func(d *Directory, depth int) string
	printTree = func(d *Directory, depth int) string {
		result := ""
		for _, dir := range d.dirs {
			for i := 0; i < depth; i++ {
				result += "\t"
			}
			result += "- " + dir.name + " (dir)\n"
			result += printTree(dir, depth+1)
		}
		for _, file := range d.files {
			for i := 0; i < depth; i++ {
				result += "\t"
			}
			size := strconv.Itoa(file.size)
			result += "- " + file.name + " (file, size=" + size + ")\n"
		}
		return result
	}
	return printTree(d, 0)
}

func (d *Directory) part1() []*Directory {
	dirs := []*Directory{}
	var run func(d *Directory)
	run = func(d *Directory) {
		if d.size() <= 100000 {
			dirs = append(dirs, d)
		}
		for _, dir := range d.dirs {
			run(dir)
		}
	}
	run(d)
	return dirs
}

const DISK_SIZE = 70000000

func (d *Directory) part2(requiredSpace int) *Directory {
	var dirToDelete *Directory

	unusedSpace := DISK_SIZE - d.size()
	requiredRemovalSize := requiredSpace - unusedSpace

	var run func(d *Directory)
	run = func(d *Directory) {
		if d.size() >= requiredRemovalSize {
			if dirToDelete == nil {
				dirToDelete = d
			} else if d.size() < dirToDelete.size() {
				dirToDelete = d
			}
		}
		for _, dir := range d.dirs {
			run(dir)
		}
	}
	run(d)
	return dirToDelete
}

func main() {
	dir, _ := os.Getwd()
	filename := dir + "/input"

	f, err := os.Open(filename)

	if err != nil {
		panic(err)
	}

	defer f.Close()

	fileScanner := bufio.NewScanner(f)
	fileScanner.Split(bufio.ScanLines)

	fs := Directory{name: "/"}
	cwd := &fs

	for fileScanner.Scan() {
		line := fileScanner.Text()
		input := strings.Split(line, " ")
		if input[0] == "$" {
			switch input[1] {
			case "cd":
				switch {
				case input[2] == "/":
					cwd = &fs
				case input[2] == "..":
					cwd = cwd.parent
				default:
					cwd = cwd.cd(input[2])
				}
			}
		} else {
			if input[0] == "dir" {
				continue
			}
			size, _ := strconv.Atoi(input[0])
			cwd.addFile(input[1], size)
		}
	}

	fmt.Println(fs.printTree())

	fmt.Println("Part 1:")
	total := 0
	for _, d := range fs.part1() {
		total += d.size()
		size := strconv.Itoa(d.size())
		fmt.Println("\t" + d.name + " - " + size)
	}
	fmt.Println("total", total)

	part2 := fs.part2(30000000)
	fmt.Println("Part 2:", part2.name, part2.size())
}
