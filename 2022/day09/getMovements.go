package main

import (
	"bufio"
	"os"
	"strconv"
	"strings"
)

type Movement struct {
	direction string
	steps     int
}

func getMovements(filename string) []Movement {
	dir, _ := os.Getwd()
	file := dir + "/" + filename

	f, err := os.Open(file)

	if err != nil {
		panic(err)
	}

	defer f.Close()

	fileScanner := bufio.NewScanner(f)
	fileScanner.Split(bufio.ScanLines)

	movments := []Movement{}
	for fileScanner.Scan() {
		line := fileScanner.Text()
		parts := strings.Split(line, " ")
		steps, _ := strconv.Atoi(parts[1])
		movments = append(movments, Movement{direction: parts[0], steps: steps})
	}
	return movments
}
