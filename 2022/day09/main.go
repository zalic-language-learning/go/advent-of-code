package main

import "fmt"

const X = 0
const Y = 1
const LENGTH = 10

type Point struct {
	x int
	y int
}

type Position map[uint8]int

type Segment struct {
	position    Position
	child       *Segment
	uniqueSteps map[Point]bool
}

func main() {
	segments := []Segment{}
	for i := 0; i < LENGTH; i++ {
		segment := Segment{
			position:    map[uint8]int{X: 0, Y: 0},
			uniqueSteps: map[Point]bool{},
		}
		if len(segments) != 0 {
			segment.child = &segments[len(segments)-1]
		}
		segments = append(segments, segment)
	}

	head := segments[len(segments)-1]

	var followSegment func(child *Segment, parent *Segment)
	followSegment = func(child *Segment, parent *Segment) {
		checkDiagonal := func(axis uint8) {
			if parent.position[axis] > child.position[axis] {
				child.position[axis]++
			} else if parent.position[axis] < child.position[axis] {
				child.position[axis]--
			}
		}
		checkAxis := func(axis uint8, otherAxis uint8) {
			dif := parent.position[axis] - child.position[axis]
			if dif > 1 {
				child.position[axis]++
				checkDiagonal(otherAxis)
			}
			if dif < -1 {
				child.position[axis]--
				checkDiagonal(otherAxis)
			}
		}
		checkAxis(X, Y)
		checkAxis(Y, X)
		child.uniqueSteps[Point{x: child.position[X], y: child.position[Y]}] = true
		if child.child != nil {
			followSegment(child.child, child)
		}
	}

	for _, movement := range getMovements("input") {
		for i := 0; i < movement.steps; i++ {
			switch movement.direction {
			case "L":
				head.position[X]--
			case "U":
				head.position[Y]++
			case "R":
				head.position[X]++
			case "D":
				head.position[Y]--
			}
			if head.child != nil {
				followSegment(head.child, &head)
			}

		}
	}

	fmt.Println("with head and tail - tail passes over passes over", len(segments[len(segments)-2].uniqueSteps), "tiles")
	fmt.Println("with "+fmt.Sprintf("%d", LENGTH)+" knots - tail passes over passes over", len(segments[0].uniqueSteps), "tiles")
}
