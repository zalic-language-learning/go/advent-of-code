package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

/**
Player1:
A for Rock,
B for Paper,
C for Scissors
Player2:
X for Lose
Y for Draw
Z for Win
Points:
1 for Rock
2 for Paper
3 for Scissors
0 lose
3 draw
6 win
*/

type CHOICE = int8

const (
	ROCK     CHOICE = iota
	PAPER           = iota
	SCISSORS        = iota
)

var ChoiceMap = map[string]CHOICE{
	"A": ROCK,
	"B": PAPER,
	"C": SCISSORS,
}

// WinChoice key - opponent choice, value my choice to win
var WinChoice = map[CHOICE]CHOICE{
	ROCK:     PAPER,
	PAPER:    SCISSORS,
	SCISSORS: ROCK,
}

// LoseChoice key - opponent choice, value my choice to lose
var LoseChoice = map[CHOICE]CHOICE{
	ROCK:     SCISSORS,
	PAPER:    ROCK,
	SCISSORS: PAPER,
}

var PointsPerChoice = map[CHOICE]int{
	ROCK:     1,
	PAPER:    2,
	SCISSORS: 3,
}

func main() {
	cwd, _ := os.Getwd()
	filename := cwd + "/input"

	f, err := os.Open(filename)

	if err != nil {
		panic(err)
	}

	defer f.Close()

	fileScanner := bufio.NewScanner(f)
	fileScanner.Split(bufio.ScanLines)

	score := 0
	for fileScanner.Scan() {
		line := fileScanner.Text()
		choices := strings.Split(line, " ")

		if len(choices) != 2 {
			log.Println(fmt.Sprintf("Invalid line in input data: '%s'", line))
			continue
		}

		p1, ok := ChoiceMap[choices[0]]
		if !ok {
			log.Fatal(choices[0], " is not a valid option")
		}

		switch choices[1] {
		case "X":
			// lose
			score += PointsPerChoice[LoseChoice[p1]]
		case "Y":
			// draw
			score += 3
			score += PointsPerChoice[p1]
		case "Z":
			// win
			score += 6
			score += PointsPerChoice[WinChoice[p1]]
		default:
			log.Fatal("Invalid expected result", choices[1])
		}
	}
	fmt.Println("score", score)
}
