package main

import (
	"fmt"
)

type Char byte

func (c Char) String() string {
	return fmt.Sprintf("%c", c)
}

type MapPoint struct {
	char Char
	used bool
}

type Result struct {
	start  Point
	end    Point
	height int
	width  int
	point  PointsMap[MapPoint]
}

func (r *Result) canStep(from, to PathPoint) bool {
	c1, err := r.point.get(from.x, from.y)
	if err != nil {
		return false
	}

	c2, err := r.point.get(to.x, to.y)
	if err != nil {
		return false
	}

	dif := int(c2.char) - int(c1.char)
	// you can only step UP max 1 blocks
	// but down infinite amount
	if dif > 1 {
		return false
	} else {
		return true
	}
}

func (r *Result) markUsed(point PathPoint) {
	mp, err := r.point.get(point.x, point.y)
	if err != nil {
		return
	}

	mp.used = true

	r.point.set(point.x, point.y, mp)
}
