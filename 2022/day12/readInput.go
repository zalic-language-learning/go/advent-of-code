package main

import (
	"io"
	"log"
	"os"
)

func readFile(filename string, result chan byte) {
	f, err := os.Open(filename)

	if err != nil {
		panic(err)
	}

	defer f.Close()

	b := make([]byte, 1)

	for {
		_, err := f.Read(b)

		if err != nil {
			if err != io.EOF {
				log.Println(err)
			}
			close(result)
			return
		}

		result <- b[0]
	}
}

func readInput(basename string) Result {
	cwd, _ := os.Getwd()
	filename := cwd + "/" + basename

	data := make(chan byte)
	go readFile(filename, data)

	var result Result
	const NEWLINE = byte('\n')
	y := 0
	x := 0
	for byte := range data {
		if byte == NEWLINE {
			result.width = x
			x = 0
			y++
			continue
		}

		if byte == 'S' {
			result.start = Point{x: x, y: y}
			result.point.set(x, y, MapPoint{char: Char('a')})
		} else if byte == 'E' {
			result.end = Point{x: x, y: y}
			result.point.set(x, y, MapPoint{char: Char('z')})
		} else {
			result.point.set(x, y, MapPoint{char: Char(byte)})
		}
		x++
	}
	result.height = y
	return result
}
