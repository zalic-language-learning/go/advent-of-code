package main

import (
	"errors"
)

type PointsMap[T interface{}] struct {
	m map[int]map[int]T
}

func (c *PointsMap[T]) set(x, y int, value T) {
	if c.m == nil {
		c.m = map[int]map[int]T{}
	}
	if c.m[x] == nil {
		c.m[x] = map[int]T{}
	}
	c.m[x][y] = value
}

func (c *PointsMap[T]) get(x, y int) (T, error) {
	if c.m == nil {
		var n T
		return n, errors.New("unset map point")
	}
	if _, ok := c.m[x]; ok {
		if value, ok := c.m[x][y]; ok {
			return value, nil
		}
	}
	var n T
	return n, errors.New("unset map point")
}
