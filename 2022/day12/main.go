package main

import (
	"fmt"

	"github.com/jwalton/go-supportscolor"
)

type Point struct {
	x int
	y int
}

type PathPoint struct {
	x int
	y int
	d Char
}

type Path []PathPoint

// https://stackoverflow.com/a/5762502
const ANSI_GREEN_BACKGROUND = "\u001B[42m"
const ANSI_RESET = "\u001B[0m"

func printPath(data *Result, path Path) {
	type PathMap struct {
		PointsMap[Char]
	}
	pmap := PathMap{}
	for i := 0; i < len(path); i++ {
		point := path[i]
		if i < len(path)-1 {
			pmap.set(point.x, point.y, path[i+1].d)
		} else {
			pmap.set(point.x, point.y, 'E')
		}
	}

	for y := 0; y < data.height; y++ {
		for x := 0; x < data.width; x++ {
			if char, err := pmap.get(x, y); err == nil {
				if supportscolor.Stdout().SupportsColor {
					fmt.Print(ANSI_GREEN_BACKGROUND)
				}
				fmt.Print(char)
				if supportscolor.Stdout().SupportsColor {
					fmt.Print(ANSI_RESET)
				}
			} else {
				point, _ := data.point.get(x, y)
				fmt.Print(point.char)
			}
		}
		fmt.Print("\n")
	}
}

func main() {
	data := readInput("input")

	var paths []Path
	paths = append(paths, Path{PathPoint{x: data.start.x, y: data.start.y}})

	walk := func(moveStart bool) Path {
		for i := 0; i <= 500; i++ {
			newPaths := []Path{}
			for _, path := range paths {
				pos := path[len(path)-1]

				possibleSteps := []PathPoint{}
				possibleSteps = append(possibleSteps, PathPoint{x: pos.x, y: pos.y - 1, d: '^'})
				possibleSteps = append(possibleSteps, PathPoint{x: pos.x, y: pos.y + 1, d: 'v'})
				possibleSteps = append(possibleSteps, PathPoint{x: pos.x - 1, y: pos.y, d: '<'})
				possibleSteps = append(possibleSteps, PathPoint{x: pos.x + 1, y: pos.y, d: '>'})

				// fmt.Println("z", len(possibleSteps))
				for i := 0; i < len(possibleSteps); i++ {
					possibleStep := possibleSteps[i]
					destination, err := data.point.get(possibleStep.x, possibleStep.y)

					if err != nil {
						continue
					}

					if !data.canStep(pos, possibleStep) {
						continue
					}

					if destination.used {
						continue
					}

					data.markUsed(possibleStep)

					newPath := make([]PathPoint, len(path))
					copy(newPath, path)
					newPath = append(newPath, PathPoint{
						x: possibleStep.x,
						y: possibleStep.y,
						d: possibleStep.d,
					})

					if moveStart {
						if len(newPath) == 2 {
							lastPP := newPath[0]
							lastMP, _ := data.point.get(lastPP.x, lastPP.y)
							if lastMP.char == 'a' {
								newPath = newPath[1:]
							}
						}
					}

					if possibleStep.x == data.end.x && possibleStep.y == data.end.y {
						return newPath
					}

					newPaths = append(newPaths, newPath)
				}
			}
			paths = newPaths
		}
		return Path{}
	}

	path := walk(true)
	if len(path) == 0 {
		fmt.Println("Could not find the path")
	} else {
		printPath(&data, path)
	}
	fmt.Println("path length", len(path)-1)
}
