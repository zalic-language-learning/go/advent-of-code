package FindStartPos

import (
	"testing"
)

func TestFindStartPosPart1(t *testing.T) {
	cases := map[string]int{
		"bvwbjplbgvbhsrlpgdmjqwftvncz":      5,
		"nppdvjthqldpwncqszvftbrmjlhg":      6,
		"nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg": 10,
		"zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw":  11,
	}
	for input, want := range cases {
		result := FindStartPos(input, 4)
		if result != want {
			t.Errorf(`FindStartPos("%s") = %d, wanted %d`, input, result, want)
		}
	}
}
func TestFindStartPosPart2(t *testing.T) {
	cases := map[string]int{
		"mjqjpqmgbljsphdztnvjfqwrcgsmlb":    19,
		"bvwbjplbgvbhsrlpgdmjqwftvncz":      23,
		"nppdvjthqldpwncqszvftbrmjlhg":      23,
		"nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg": 29,
		"zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw":  26,
	}
	for input, want := range cases {
		result := FindStartPos(input, 14)
		if result != want {
			t.Errorf(`FindStartPos("%s") = %d, wanted %d`, input, result, want)
		}
	}
}
