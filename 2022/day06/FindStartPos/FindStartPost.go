package FindStartPos

func isUniqueChars(input string) bool {
	unique := map[uint8]bool{}
	for i := 0; i < len(input); i++ {
		if _, ok := unique[input[i]]; !ok {
			unique[input[i]] = true
		} else {
			return false
		}
	}
	return true
}

func FindStartPos(input string, uniqueChars int) int {
	l := len(input)
	if l < uniqueChars {
		return -1
	}
	for i := uniqueChars; i < l; i++ {
		if isUniqueChars(input[i-uniqueChars : i]) {
			return i
		}
	}
	return 0
}
