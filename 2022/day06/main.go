package main

import (
	"day06/FindStartPos"
	"fmt"
	"os"
)

func main() {
	cwd, _ := os.Getwd()
	filename := cwd + "/input"

	fmt.Println("reading input file", filename)

	f, _ := os.ReadFile(filename)
	fmt.Println("part1", FindStartPos.FindStartPos(string(f), 4))
	fmt.Println("part2", FindStartPos.FindStartPos(string(f), 14))
}
