package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func parseLine(line string) []int {
	result := []int{}
	for _, char := range strings.Split(line, "") {
		n, _ := strconv.Atoi(char)
		result = append(result, n)
	}
	return result
}

type TreeMap [][]int

func (m TreeMap) treesToTheLeft(y, x int) []int {
	trees := make([]int, 0)
	for i := x - 1; i >= 0; i-- {
		trees = append(trees, m[y][i])
	}
	return trees
}
func (m TreeMap) treesAbove(y, x int) []int {
	trees := make([]int, 0)
	for i := y - 1; i >= 0; i-- {
		trees = append(trees, m[i][x])
	}
	return trees
}
func (m TreeMap) treesToTheRight(y, x int) []int {
	trees := make([]int, 0)
	for i := x + 1; i < len(m); i++ {
		trees = append(trees, m[y][i])
	}
	return trees
}
func (m TreeMap) treesBelow(y, x int) []int {
	trees := make([]int, 0)
	for i := y + 1; i < len(m); i++ {
		trees = append(trees, m[i][x])
	}
	return trees
}

func visibleTreeCount(treeHeight int, view []int) int {
	count := 0
	for _, height := range view {
		count++
		if height >= treeHeight {
			break
		}
	}
	return count
}

func (m TreeMap) scenicScore(y, x int) int {
	treeHeight := m[y][x]
	left := visibleTreeCount(treeHeight, m.treesToTheLeft(y, x))
	above := visibleTreeCount(treeHeight, m.treesAbove(y, x))
	right := visibleTreeCount(treeHeight, m.treesToTheRight(y, x))
	below := visibleTreeCount(treeHeight, m.treesBelow(y, x))
	return left * above * right * below
}

func isTreeHidden(treeHeight int, view []int) bool {
	for _, height := range view {
		if height >= treeHeight {
			return true
		}
	}
	return false
}

func (m TreeMap) isVisible(y, x int) bool {
	treeHeight := m[y][x]
	left := isTreeHidden(treeHeight, m.treesToTheLeft(y, x))
	above := isTreeHidden(treeHeight, m.treesAbove(y, x))
	right := isTreeHidden(treeHeight, m.treesToTheRight(y, x))
	below := isTreeHidden(treeHeight, m.treesBelow(y, x))
	return !left || !above || !right || !below
}

func main() {
	dir, _ := os.Getwd()
	filename := dir + "/input"

	f, err := os.Open(filename)

	if err != nil {
		panic(err)
	}

	defer f.Close()

	fileScanner := bufio.NewScanner(f)
	fileScanner.Split(bufio.ScanLines)

	var treeMap TreeMap = make([][]int, 0)
	for fileScanner.Scan() {
		line := fileScanner.Text()
		treeMap = append(treeMap, parseLine(line))
	}

	maxY := len(treeMap) - 1
	maxX := len(treeMap[0]) - 1

	visibleTreeCount := 0
	for y, row := range treeMap {
		for x := range row {
			if y == 0 || x == 0 || y == maxY || x == maxX {
				// this tree is is on the side
				visibleTreeCount++
				continue
			}

			if treeMap.isVisible(y, x) {
				visibleTreeCount++
			}
		}
	}

	fmt.Println("number of visible trees", visibleTreeCount)

	bestScenicScore := 0
	for y, row := range treeMap {
		for x := range row {
			if y == 0 || x == 0 || y == maxY || x == maxX {
				continue
			}
			scenicScore := treeMap.scenicScore(y, x)
			if scenicScore > bestScenicScore {
				bestScenicScore = scenicScore
			}
		}
	}
	fmt.Println("Best Scenic Score", bestScenicScore)

}
