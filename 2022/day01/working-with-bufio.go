package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

/*
one item per line
each elf separated by empty line

Find the Elf carrying the most Calories ( how many calories )
*/

func main() {
	cwd, _ := os.Getwd()
	filename := cwd + "/input"

	fmt.Println("reading input file", filename)
	f, err := os.Open(filename)

	if err != nil {
		panic(err)
	}

	fileScanner := bufio.NewScanner(f)
	fileScanner.Split(bufio.ScanLines)

	var (
		c   int
		max int
	)

	for fileScanner.Scan() {
		line := fileScanner.Text()
		if line == "" {
			if c > max {
				max = c
			}
			c = 0
		} else {
			n, err := strconv.Atoi(line)

			if err != nil {
				log.Println("one of the lines was not a valid number: ", err)
			}

			c += n
		}
	}

	f.Close()

	fmt.Println(max)
}
