package main

import (
	"fmt"
	"log"
	"os"
	"strconv"
)

/*
one item per line
each elf separated by empty line

Find the Elf carrying the most Calories ( how many calories )
*/

const SEPARATOR_LF = byte('\n')

// const SEPARATOR_CR = byte('\r')

func main() {
	cwd, _ := os.Getwd()
	filename := cwd + "/input"

	fmt.Println("reading input file", filename)

	f, _ := os.ReadFile(filename)

	var (
		c   int
		max int
	)

	index := 0
	for i := 0; i < len(f); i++ {

		// if you want to support \r\n and \r linebreaks too
		// if f[i] != SEPARATOR_LF && f[i] != SEPARATOR_CR {
		if f[i] != SEPARATOR_LF {
			continue
		}

		if index == i {
			if c > max {
				max = c
			}
			c = 0
			index = i + 1
			continue
		}

		n, err := strconv.Atoi(string(f[index:i]))

		if err != nil {
			log.Println(err)
		}

		index = i + 1
		c += n
	}

	fmt.Println(max)
}
