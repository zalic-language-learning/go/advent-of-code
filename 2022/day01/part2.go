package main

/*
 get Calories carried by the top three Elves carrying the most Calories
*/

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

type Result [3]int

var result Result

func (result Result) add(number int) Result {
	var addValue func(number, index int) Result

	addValue = func(number, index int) Result {
		if index >= cap(result) {
			return result
		}

		currentValue := result[index]

		if currentValue < number {
			result[index] = number
			number = currentValue
		}

		return addValue(number, index+1)
	}

	return addValue(number, 0)
}

func (result Result) sum() int {
	sum := 0
	for _, value := range result {
		sum += value
	}
	return sum
}

func main() {
	cwd, _ := os.Getwd()
	filename := cwd + "/input"

	fmt.Println("reading input file", filename)
	f, err := os.Open(filename)

	if err != nil {
		panic(err)
	}

	fileScanner := bufio.NewScanner(f)
	fileScanner.Split(bufio.ScanLines)

	var totalPerElf int
	for fileScanner.Scan() {
		line := fileScanner.Text()
		if line == "" {
			result = result.add(totalPerElf)
			totalPerElf = 0
		} else {
			n, err := strconv.Atoi(line)

			if err != nil {
				log.Println("one of the lines was not a valid number: ", err)
			}

			totalPerElf += n
		}
	}

	f.Close()

	fmt.Println(result, result.sum())
}
