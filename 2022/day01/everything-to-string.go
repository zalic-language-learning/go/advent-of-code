package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

/*
one item per line
each elf separated by empty line

Find the Elf carrying the most Calories ( how many calories )
*/

func main() {
	cwd, _ := os.Getwd()
	filename := cwd + "/input"

	fmt.Println("reading input file", filename)

	f, _ := os.ReadFile(filename)
	s := string(f)

	var (
		c   int
		max int
	)

	lines := strings.Split(s, "\n")
	for _, line := range lines {
		if line == "" {
			if c > max {
				max = c
			}
			c = 0
		} else {
			n, _ := strconv.Atoi(line)
			c += n
		}
	}

	fmt.Println(max)
}
