package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
)

/*
*
- https://stackoverflow.com/a/61528275
- https://cs.opensource.google/go/go/+/refs/tags/go1.20.5:src/os/file.go;l=674
*/
func readFile(filename string, out chan byte) {
	f, err := os.Open(filename)

	if err != nil {
		panic(err)
	}

	defer f.Close()

	b := make([]byte, 1)

	for {
		_, err := f.Read(b)

		if err != nil {
			if err != io.EOF {
				log.Println(err)
			}
			close(out)
			return
		}

		out <- b[0]
	}
}

func main() {
	cwd, _ := os.Getwd()
	filename := cwd + "/input"

	fmt.Println("reading input file", filename)

	data := make(chan byte)
	go readFile(filename, data)

	const NEWLINE = byte('\n')
	var (
		c   int
		max int
	)

	buffer := []byte{}
	for byte := range data {
		if byte == NEWLINE {
			if len(buffer) == 0 {
				if c > max {
					max = c
				}
				c = 0
			} else {
				n, err := strconv.Atoi(string(buffer))

				if err != nil {
					log.Println(err)
				}

				c += n

				buffer = nil
			}
			continue
		}
		buffer = append(buffer, byte)
	}
	fmt.Println(max)
}
